stm8/

three equ 3

    segment 'rom'

.delay:
    ld a, #$06
    ldw y, #$1a80               ; 0x61a80 = 400000 i.e. (2*10^6 MHz)/5cycles
loop:
    subw y, #$01                ; decrement with set carry
    sbc a,#0                    ; decrement carry flag i.e. a = a - carry_flag
    jrne loop
    ret	
.add_uint32
	; input parementers should by in stack: sp+7 and sp+11
	; output to sp+3
	push a
	ld a,(11,sp)
	add a,(15,sp)
	ld (7,sp),a
	ld a,(10,sp)
	adc a,(14,sp)
	ld (6,sp),a
	ld a,(9,sp)
	adc a,(13,sp)
	ld (5,sp),a
	ld a,(8,sp)
	adc a,(12,sp)
	ld (4,sp),a	
	pop a
	ret
	
add_uint32_idx
	; input parameters: x pointer of number1, y pointer of number2,
	; difference address to x
	push a
	ld a,(3,x)
	add a,(3,y)
	ld (3,x),a
	ld a,(2,x)
	adc a,(2,y)
	ld (2,x),a
	ld a,(1,x)
	adc a,(1,y)
	ld (1,x),a
	ld a,(x)
	adc a,(y)
	ld (x),a	
	pop a
	ret

.sub_uint32
	; input parameters: x address of minued, y address of subtrahend,
	; difference address to x
	push a
	ld a,(11,sp)
	sub a,(15,sp)
	ld (7,sp),a
	ld a,(10,sp)
	sbc a,(14,sp)
	ld (6,sp),a
	ld a,(9,sp)
	sbc a,(13,sp)
	ld (5,sp),a
	ld a,(8,sp)
	sbc a,(12,sp)
	ld (4,sp),a	
	pop a
	ret
	
sub_uint32_idx
	; input parameters: x address of minued, y address of subtrahend,
	; difference address to x
	push a 
	ld a,(3,x)
	sub a,(3,y)
	ld (3,x),a
	ld a,(2,x)
	sbc a,(2,y)
	ld (2,x),a
	ld a,(1,x)
	sbc a,(1,y)
	ld (1,x),a
	ld a,(x)
	sbc a,(y)
	ld (x),a
	pop a
	ret

	
.calc_date
	; output date to sp+3(8bit), month to sp+4(8bit), year to sp+5(8bit), day to sp+6(8bit)
	subw sp,#8
	; ------ calculate WD --------------
	ldw y,(11,sp)
	ldw (1,sp),y
	ldw y,(13,sp)	
	ldw (3,sp),y		; mdj to var1
	clrw y				; var2=2
	ldw (5,sp),y
	ldw y,#2
	ldw (7,sp),y		; var2=2
	ldw x,sp
	incw x				; pointer of var1
	ldw y,sp
	addw y,#5			; pointer of var1
	call add_uint32_idx ; var1=mdj +2
	ldw y,#7
	call uint24_div		; WD = var1 / 7
	ld a,yl
	push a				; save WD
	; ------ calculate Y' --------------
	; mjd to var1
	ldw x,(12,sp)
	ldw (2,sp),x
	ldw x,(14,sp)
	ldw (4,sp),x
	; multiply
	ldw x,sp
	addw x,#2
	ld a,#100
	call uint24_mult		; var1 = mdj * 100
	ldw x,#$17
	ldw (6,sp),x
	ldw x,#$01ec
	ldw (8,sp),x			; var2 =1507820
	ldw x,sp
	ldw y,sp
	addw x,#2
	addw y,#6
	call sub_uint32_idx     ; var1 -=var2
	;var1 /= 36525, where  36525 = 487*75
	ldw y,#$1e7				; =487
	call uint24_div		
	ldw y,#75
	call uint24_div
	ld a, (5,sp)			; save Y' in stack
	push a
	; ------ calculate M' -------------
	;clrw y					; var1 = mjd
	ldw x,(13,sp)
	ldw (3,sp),x
	ldw x,(15,sp)
	ldw (5,sp),x
	; multiply
	ldw x,sp
	addw x,#3				; set pointer var1
	ld a,#100
	call uint24_mult		; var1 *= 100
	pushw x
	ldw x,#$16
	ldw (9,sp),x
	ldw x,#$d23a
	ldw (11,sp),x			; var2 =1495610
	popw x
	ldw y,sp
	addw y,#7				; set pointer var2
	call sub_uint32_idx     ; var1 -= var2
	clrw x
	ldw (7,sp),x
	ldw x,#$8ead			; var2 = 36525
	ldw (9,sp),x
	ld a,(1,sp)				; A = Y'
	ldw x,sp
	addw x,#7
	call uint24_mult		; var2 *= Y'
	subw x,#4				; x = var1, y = var2
	call sub_uint32_idx		; var1 -= var2
	ldw y,#100				; rounding
	call uint24_div			; var1 =/ 100
	ld a,#100
	call uint24_mult		; var1 *= 10000
	call uint24_mult
	ldw y,#$1f				; var1 /= 306001, where 306001=31*9871
	call uint24_div
	ldw y,#$268f
	call uint24_div
	ld a,(3,x)
	push a					; save M'
	; ------ calculate D -------------
	ldw y,(14,sp)
	ldw (x),y
	ldw y,(16,sp)
	ldw (2,x),y				; var1 = mjd
	clrw y
	ldw (4,x),y
	ldw y,#$3a6c
	ldw (6,x),y				; var2 =14956
	ldw y,x
	addw y,#4
	call sub_uint32_idx		; var1 -=var2
	pushw x
	pushw y
	clrw x
	ldw (y),x
	ldw x,#$8ead			; var2 =36525
	ldw (2,y),x
	exgw x,y				; x = var2
	ld a,(6,sp)				; A = Y'
	call uint24_mult		; var2 = 36525 * Y'
	ldw y,#100
	call uint24_div			; var2 /= 100
	popw y
	subw x,#4
	call sub_uint32_idx		; var1 -=var2
	clrw x
	ldw (y),x
	ldw x,#$268f			; var2 =9871
	ldw (2,y),x
	pushw y
	exgw x,y				; x=var2
	ld a,(5,sp)				; A = M'
	call uint24_mult		; var2 = M' * 306001
	ld a,#$1f
	call uint24_mult		
	ldw y,#100
	call uint24_div
	ldw y,#100
	call uint24_div			; var2 /= 10000
	popw y
	popw x
	call sub_uint32_idx		; var1 -= var2
	ld a,(3,x)
	push a					; save D
	; ------ calculate K -------------
	ld a,(2,sp)				; A= M'
	push #0					; K=0
	cp a,#14
	jrmi zeroK				; if (M'<14) then K=0
	inc (1,sp)				; else M'=1
zeroK:
	; ------ calculate Y -------------
	ld a,(4,sp)				; A=Y'
	add a,(1,sp)			; A=Y'+K
	ld (4,sp),a				; save Y
	; ------ calculate M -------------
	dec (3,sp)
	pop a
	clrw x
	ld xl,a
	ld a,#12
	mul x,a
	ld a,xl
	push a
	ld a, (3,sp)
	sub a,(1,sp)
	ld (3,sp),a
	; ---- save result ----------------
	ld a,(2,sp)				; Day
	ld (16,sp),a
	ld a,(3,sp)				; Month
	ld (17,sp),a
	ld a,(4,sp)
	ld (18,sp),a			; Year
	ld a,(5,sp)			
	ld (19,sp),a			; Week Day
	;----------------------------------
	addw sp,#13				; aligning stack
	ret

.uint24_div:
	; subroutine for divide 24bit integer number by a 16bit integer number
	; 24bit number(dividend) must have 4 bytes. highest byte not used in calculate
	; input parameters: X - pointer for 32-bit unsigned number(Quotient)
	; 					Y - 16-bit divisor
	; output paremeter: X - pointer fo quotient 
	;					Y - remainder
	push a			; save accumulator
	pushw x			; save X registor  <---------
	pushw y			; save Y registor			|
	ldw x,(x)
	tnzw x
	jrne div_begin
	ldw x,(3,sp)
	ldw x,(2,x)
	divw x,y
	pushw x
	ldw x,(5,sp)
	ld a,(1,sp)
	ld (2,x),a
	ld a,(2,sp)
	ld (3,x),a
	addw sp,#4
	popw x
	pop a
	ret
div_begin:
	ldw x,(3,sp)		; load pointer
	ld a,(3,x)			; low byte to accumulator
	push a				; save low byte
	push #0				; counter=0, local variable
div_left_shift:
	ld a,(1,x)
	jrmi div_main
	sll (3,x)
	rlc (2,x)
	rlc (1,x)
	inc (1,sp)
	sll (2,sp)
	jra div_left_shift
div_main:
	ldw x,(1,x)		; load high value			|
	divw x,y		; divide, step 1			|
	pop a			; counter
	push #0
	pushw x			; save quotient in stack    |
	push #0
	push #0
	pushw y
div_right_shift:
	tnz a
	jreq end_shift
	dec a
	srl (1,sp)
	rrc (2,sp)
	rrc (3,sp)
	srl (5,sp)
	rrc (6,sp)
	rrc (7,sp)
	srl (8,sp)
	jra div_right_shift
end_shift:
	ld a,(8,sp)
	add a,(3,sp)
	ld (3,sp),a
	ld a,(2,sp)
	adc a,#0
	ld (2,sp),a
	ld a,(1,sp)
	adc a,#0
	ld (1,sp),a
	push #0
	ldw x,sp
	incw x
	ldw y,(10,sp)
	call uint24_div
	;push #0
	pushw y
	ldw x,sp
	addw x,#3
	ldw y,x
	addw y,#4
	call add_uint32_idx
	ldw y,(3,sp)
	ldw x,(14,sp)
	ldw (x),y
	ldw y,(5,sp)
	ldw (2,x),y

	; end of subroutine
	popw y		
	addw sp,#13
	pop a
	ret
	
uint48_right_shift:
	; input parameters: X - pointer for 48-bit unsigned number
	; 					A - count of steps
	push a
uint48_rshift_loop:
	srl (5,x)
	rrc (4,x)
	rrc (3,x)
	rrc (2,x)
	rrc (1,x)
	rrc (x)
	dec a
	jreq uint48_rshift_loop
	pop a
	ret

uint48_add
	; input parameters: x pointer of number1, y pointer of number2,
	; difference address to x
	push a
	pushw y
	pushw x
	addw x,#5
	addw y,#5
	push #6
	rcf					; reset carry flag
uint48_add_loop:
	ld a,(x)
	adc a,(y)
	ld (x),a
	dec (1,sp)
	decw x
	decw y
	jrne uint48_add_loop	
	pop a
	popw x
	popw y
	pop a
	ret
	
uint48_mult:
	; subroutine for multiply two 24bit unsigned numbers 
	; 24bit number must have 4 bytes. highest byte not used in calculate
	; input parameters: X - pointer for 32-bit unsigned number
	; 					A - 8-bit unsigned scaler
	; output parameter: X - pointer for result of multiply
	pushw x
	pushw y
	push a
	subw sp,#12
	;push 
	addw y,#3
	ld a,(y)
	pushw x
	ldw x,sp
	addw x,#10
	
	
	
	
	
	
	
	pop a
	popw y
	popw x
	ret
	
	

uint24_mult:
	; subroutine for multiply 24bit integer number with 8bit integer number
	; 24bit number must have 4 bytes. highest byte not used in calculate
	; input parameters: X - pointer for 32-bit unsigned number
	; 					A - 8-bit unsigned scaler
	; output parameter: X - pointer for result of multiply
	pushw y
	push a				; save scaler
	push #three			; three steps
	addw x,#three		; calculate three bytes
	push #0
uint24_mult_loop:	
	ld a,(x)			; get current byte
	ld yl,a				; y=a
	ld a,(3,sp)			; get scaler
	mul y,a				; multiply
	ld a,yl
	add a,(1,sp)		; low byte of result multiply add with high byte previous step
	ld (x),a			; write result
	decw x				
	;pop a				; aligning of stack
	ld a,yh
	adc a,#0			; add carry flag to high byte of result
	;push a
	ld (1,sp),a
	dec (2,sp)
	jrne uint24_mult_loop	; go to next step
	addw sp,#2
	pop a
	popw y
	ret

.myadd
	pushw x
	ldw x,(15,sp)
	addw x,(11,sp)
	ldw (7,sp),x
	ldw x,(13,sp)
	jrnc myadd_nocarry
	addw x,(9,sp)
	incw x
	jra myadd_carry
myadd_nocarry:
	addw x,(9,sp)
myadd_carry:
	ldw (5,sp),x
	popw x
	ret
    end
	