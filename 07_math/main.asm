stm8/
    #include "STM8S103F.inc"
	extern delay
	extern calc_date, uint24_div:

LED equ 5

    segment 'rom'

.main
    bset PB_DDR, #LED       ; PB_DDR|=(1<<LED)
    bset PB_CR1, #LED       ; PB_CR1|=(1<<LED)
mloop:
    bcpl PB_ODR, #LED       ; PB_ODR^=(1<<LED)
	;---- debug ---------
	mov 0,#$0
	mov 1,#$11
	mov 2,#$fb
	mov 3,#$1a
	ldw y,#$10ab
	clrw x
	call uint24_div
	;---- end debug -----
	ldw x,#$e661			; mjd=58977, this is 8 may 2020
	;ldw x,#$e678			; mjd=59000, rhis is 31 may 2020
	pushw x
	ldw x,#0
	pushw x
	call calc_date
	addw sp,#4

    jp mloop

    end
	