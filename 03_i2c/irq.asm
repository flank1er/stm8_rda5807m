stm8/
	INTEL
	extern main
    #include "mapping.inc"
	#include "STM8S103F.inc"

LEN	equ 10
EOL equ	LEN			; =Zero always
;-------- Variables ----------------------
STR	equ 0					; buffer[10bytes]
INDEX	cequ	{EOL+1}	; 1 byte
READY	cequ	{INDEX+1}	; 1 byte

    segment 'rom'

reset.l
    ; initialize SP
    ldw X,#03ffh
    ldw SP,X
    jp main
    jra reset

    interrupt NonHandledInterrupt
NonHandledInterrupt.l
	jra NonHandledInterrupt
    iret    

UART_RX_IRQ.l:
    btjt UART1_SR,#3,CLEAR_OR_FLAG      ; if OR flag is set
    ld a,UART1_DR                       ; get received byte and clear RXNE flag
    cp a,#0dh                            ; if received char == CR
    jreq NULL_Terminate
    cp a,#0ah                            ; if received char == '\n'
    jreq NULL_Terminate
    ld yl,a                            	; store received char
    ld a,INDEX                          ; get index
    cp a,#LEN                           ; if index is over  
    jreq QUIT
    ld a,yl								; restore received char  	 
	ld [EOL.w],a
    inc INDEX
    iret
CLEAR_OR_FLAG:                          ; if OR flag is set, then clear it
    ld a,UART1_DR
    ld a,UART1_SR
NULL_Terminate:
	clr [EOL]							; set NULL/EOL
    ; set READY flag for main loop
QUIT:
    bset READY,#0                       ; was received EOL
    iret
	MOTOROLA
    segment 'vectit'
    dc.l {$82000000+reset}                  ; reset
    dc.l {$82000000+NonHandledInterrupt}    ; trap
    dc.l {$82000000+NonHandledInterrupt}    ; irq0
    dc.l {$82000000+NonHandledInterrupt}    ; irq1
    dc.l {$82000000+NonHandledInterrupt}    ; irq2
    dc.l {$82000000+NonHandledInterrupt}    ; irq3
    dc.l {$82000000+NonHandledInterrupt}    ; irq4
    dc.l {$82000000+NonHandledInterrupt}    ; irq5
    dc.l {$82000000+NonHandledInterrupt}    ; irq6
    dc.l {$82000000+NonHandledInterrupt}    ; irq7
    dc.l {$82000000+NonHandledInterrupt}    ; irq8
    dc.l {$82000000+NonHandledInterrupt}    ; irq9
    dc.l {$82000000+NonHandledInterrupt}    ; irq10
    dc.l {$82000000+NonHandledInterrupt}    ; irq11
    dc.l {$82000000+NonHandledInterrupt}    ; irq12
    dc.l {$82000000+NonHandledInterrupt}    ; irq13
    dc.l {$82000000+NonHandledInterrupt}    ; irq14
    dc.l {$82000000+NonHandledInterrupt}    ; irq15
    dc.l {$82000000+NonHandledInterrupt}    ; irq16
    dc.l {$82000000+NonHandledInterrupt}    ; irq17
    dc.l {$82000000+UART_RX_IRQ}    		; irq18
    dc.l {$82000000+NonHandledInterrupt}    ; irq19
    dc.l {$82000000+NonHandledInterrupt}    ; irq20
    dc.l {$82000000+NonHandledInterrupt}    ; irq21
    dc.l {$82000000+NonHandledInterrupt}    ; irq22
    dc.l {$82000000+NonHandledInterrupt}    ; irq23
    dc.l {$82000000+NonHandledInterrupt}    ; irq24
    dc.l {$82000000+NonHandledInterrupt}    ; irq25
    dc.l {$82000000+NonHandledInterrupt}    ; irq26
    dc.l {$82000000+NonHandledInterrupt}    ; irq27
    dc.l {$82000000+NonHandledInterrupt}    ; irq28
    dc.l {$82000000+NonHandledInterrupt}    ; irq29

        END
		