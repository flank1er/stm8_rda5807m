stm8/
    #include "STM8S103F.inc"
	extern delay,uart1_print_str,uart1_print_num,uart1_print_char,strcmp,strnum
	extern strfrac,check_cmd, uart1_print_str_nl
	extern rda5807m_update,rda5807m_control_write, rda5807m_write_register

print_nl MACRO
	ld a, #$a              
    call uart1_print_char
	MEND
print_str MACRO msg
	ldw x, #msg
	call uart1_print_str
	MEND
print_str_nl MACRO msg
	ldw x, #msg
	call uart1_print_str_nl
	MEND
	
LED equ 5
LEN	equ 10
EOL equ	LEN			; =Zero always
;-------- Variables ----------------------
STR	equ 0					; buffer[10bytes]
INDEX	cequ	{EOL+1}	; 1 byte
READY	cequ	{INDEX+1}	; 1 byte
RDA_STAT cequ   {READY+1}
;-------- Constants ----------------------
RDA5807_CTRL equ $10
RDA5807M_SEQ_I2C_ADDRESS equ $20
RDA5807M_RND_I2C_ADDRESS equ $22
RDA5807M_CTRL_REG equ $02
RDA5807M_CMD_RESET equ $0002
;------------------------------------------

    segment 'rom'
.main
    ;----------- Setup Clock ----------------------
    ; Setup fHSI = 16MHz
    clr CLK_CKDIVR
    ; Enable UART and I2C,  turn off other  peripherals   
    mov CLK_PCKENR1, #0
    mov CLK_PCKENR2, #0
    bset CLK_PCKENR1, #3    ; enable UART1
	bset CLK_PCKENR1, #0	; enable I2C
    ;----------- Setup GPIO -----------------------
    ;bset PB_DDR, #LED       ; PB_DDR|=(1<<LED)
    ;bset PB_CR1, #LED       ; PB_CR1|=(1<<LED)
    ;----------- Setup UART1 ----------------------
    ; Clear
    clr UART1_CR1
    clr UART1_CR2
    clr UART1_CR3
    clr UART1_CR4
    clr UART1_CR5
    clr UART1_GTR
    clr UART1_PSCR
    ; Setup UART1, set 115200 Baud Rate
    bset UART1_CR1, #5      ; set UARTD, UART1 disable
    ; 9600 Baud Rate
    ;mov UART1_BRR2, #0x03
    ;mov UART1_BRR1, #0x68
    ; 115200 Baud Rate
    mov UART1_BRR2, #$0b
    mov UART1_BRR1, #$08   
    ; 230400 Baud Rate
    ;mov UART1_BRR2, #0x05
    ;mov UART1_BRR1, #0x04
    ; 921600 Baud Rate
    ;mov UART1_BRR2, #0x01
    ;mov UART1_BRR1, #0x01
    ; Trasmission Enable
	bset UART1_CR2, #3      ; set TEN, Transmission Enable
    bset UART1_CR2, #2      ; set REN, Receiver Enable
    bset UART1_CR2, #5      ; set RIEN, Enable Receiver Interrupt 
    ; enable UART1
    bres UART1_CR1, #5      ; clear UARTD, UART1 enable
	;------------- I2C Setup ----------------------
	bres I2C_CR1,#0			; PE=0, disable I2C before setup
	mov I2C_FREQR,#16		; peripheral frequence =16MHz
	clr I2C_CCRH			; =0
	mov I2C_CCRL,#80		; 100kHz for I2C
	bres I2C_CCRH,#7		; set standart mode(100�Hz)
	bres I2C_OARH,#7		; 7-bit address mode
	bset I2C_OARH,#6		; see reference manual
    ;------------- End Setup ---------------------
	clr EOL					;set NULL/EOL
	ldw x,#$40	
clear:
	clr (x)
	decw x
	jrne clear
	mov RDA_STAT,#1
    ; let's go...
    rim                     	; enable Interrupts
start:
    clr INDEX               	; INDEX=0
    clr READY               	; READY=0
mloop:
    btjt READY,#0,mute			; if buffer not empty
	jp blink					; if buffer empty
mute:
	; CHECK: if was recived "mute" command
	clrw x						; ard of buffer: x=0
	ldw y,#cmd_mute
	call strcmp					; check incoming command
	jrne unmute	
	bres $10,#6					; clear DMUTE bit
	ldw x,$10					; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write	; write to REG_02
	addw sp,#2
	bset RDA_STAT,#0			; update
	print_str msg_mute			; print info message
	jp start					; break
unmute:
	; CHECK: if was recived "mute" command
	clrw x
	ldw y,#cmd_unmute
	call strcmp					; check incoming command
	jrne set_vol
	bset $10,#6					; set DMUTE bit
	ldw x,$10					; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write ; write to REG_02
	addw sp,#2
	bset RDA_STAT,#0			; update
	print_str msg_unmute		; print info message
	jp start					; break
set_vol:
	push #'v'
	push #'='
	call check_cmd				; check incoming command
	jrne vol_down				; if Z not set, then check next command
	ldw x,#02
	call strnum					; get integer part
	and a,#$0f					; mask parameter
	ldw x,$16					; x=REG_5 /VOLUME/
	push a
	ld a,xl
	and a,#$f0					; x=(x & 0xfff0)
	or a,(1,sp)					; x=(x | num)
	ld xl,a						
	pop a
	pushw x
	push #05					; select REG_5 to write
	call rda5807m_write_register; write volume to REG_5 /VOLUME/
	addw sp,#03
	bset RDA_STAT,#0			; update
	jp start					; break
vol_down:
	push #'v'
	push #'-'
	call check_cmd				; check incoming command
	jrne vol_up					; next
	ld a,$17					; a=ram[0x17] (low byte REG_05 /VOLUME/)
	and a,#$0f					; mask
	jreq vol_min				; if current volume is minimum(=0)
	;------------
	print_str msg_volume		; print info message  "volume="
	clrw x						; x=0
	dec a						; volume -=1
	ld xl,a						; x=a
	call uart1_print_num		; print volume
	print_nl					; print NL
	;----------------
	ldw x,$16					; x=REG_05 /VOLUME/
	decw x						; volume down
	pushw x
	push #05
	call rda5807m_write_register; write X to REG_05 /VOLUME/
	addw sp,#03
	bset RDA_STAT,#0			; update
	jp start					; break
vol_min:
	print_str msg_volume_min	; print error message
	jp start
vol_up:
	push #'v'
	push #'+'
	call check_cmd				; check incoming command
	jrne seek_down				; next
	ld a,$17					; a=ram[0x17] (low byte REG_05 /VOLUME/)
	and a,#$0f					; mask
	cp a,#$0f					; if (a==15)
	jreq vol_max				; if current volume is maximum(=15)
	;------------
	print_str msg_volume		; print info message "volume="
	clrw x
	inc a
	ld xl,a	
	call uart1_print_num		; print volume
	print_nl					; print NL
	;----------------
	ldw x,$16					; x=REG_05 /VOLUME/
	incw x						; vlume up
	pushw x
	push #05
	call rda5807m_write_register; write X to REG_05 /VOLUME/
	addw sp,#03
	bset RDA_STAT,#0			; set "to update" flag
	jp start					; break
vol_max:
	print_str msg_volume_max	; print error message
	jp start
seek_down:
	push #'s'
	push #'-'
	call check_cmd				; check incoming command
	jrne seek_up				; next 
	bres $10,#1					; seek-down
	bset $10,#0					; seek enable
	ldw x,$10					; X = ram[0x10]
	pushw x
	call rda5807m_control_write ; write X to REG_02 /CONTROL/
	addw sp,#2
	bset RDA_STAT,#0			; set "to update" flag
	print_str msg_seekdown		; print info message
	jp start					; break
seek_up:
	push #'s'
	push #'+'
	call check_cmd				; check incoming command
	jrne on_cmd					; next
	bset $10,#1					; seek-up
	bset $10,#0					; seek enable
	ldw x,$10					; X = ram[0x10]
	pushw x
	call rda5807m_control_write ; write X to REG_02 /CONTROL/
	addw sp,#2
	bset RDA_STAT,#0			; set "to update" flag
	print_str msg_seekup		; print info message
	jp start					; break
on_cmd:
	push #'o'
	push #'n'
	call check_cmd				; check incoming command
	jrne freq					; next
	print_str msg_on			; print info message
	ldw x,$10
	ld a,xl
	or a,#RDA5807M_CMD_RESET    ; set RESET bit
	ld xl,a
	pushw x
	call rda5807m_control_write ; write to REG_02 /CONTROL/
	addw sp,#2
	ldw x,#$c10d				; REG_02=0xC10D (Turn_ON + SEEK)
	pushw x
	call rda5807m_control_write ; write to REG_02 /CONTROL/
	addw sp,#2
	bset RDA_STAT,#0			; update
	jp start					; check incoming command
freq:
	; CHECK: if was recived "f=NUM.NUM" command
	push #'f'
	push #'='
	call check_cmd				; check
	jrne help					; if not "f=" then goto help
	ldw x,#02
	call strnum					; get integer part
	subw sp,#2
	ld ($1,sp),a				; (sp+1)=integer part
	call strfrac				; get fractional part
	ld ($2,sp),a				; (sp+2)=fractional part
	print_str msg_freq			; print "freq="
	ld a,($1,sp)
	clrw x
	ld xl,a						; x=interger part
	call uart1_print_num		; print x
	ld a, #'.'
    call uart1_print_char		; print dot
	ld a,($2,sp)
	ld xl,a						; x=fractional part
	call uart1_print_num		; print x
	print_nl					; NewLine
	addw sp,#2
	jp start					; break
help:
	; CHECK: if was recived "?" command
	clrw x						; if "?"
	ld a,(x)
	cp a,#'?'					; check incoming command
	jrne help_2
	incw x
	ld a,(x)
	cp a,#0						; NULL 
	jrne help_2
	print_str msg_help			; print help message
	jp start					; break
help_2:
	clrw x
	ldw y,#cmd_help				
	call strcmp					; check incoming command
	jrne volume
	print_str msg_help			; print help message
	jp start					; break
volume:
	; get current Gain Control Bits(volume)
	push #'?'
	push #'v'
	call check_cmd				; check incoming command
	jrne next					; if not "?v" then goto next
	print_str msg_volume		; print "volume="
	ld a,$17					; load low byte of REG_5 /VOLUME/
	and a,#$0f					; mask
	clrw x
	ld xl,a						; x = a
	call uart1_print_num		; print volume
	print_nl					; NewLine
next:
	; ----- END OF CASE ---------------
	jp start
	
print_command:
	print_str msg_command
	clrw x
	call uart1_print_str_nl		; print incoming string
	jp start
blink:
	btjf RDA_STAT,#0, blink_delay
	bres RDA_STAT,#0
	call rda5807m_update		; read rda5807m
	tnz a
	jrne blink_delay
	print_str msg_update
blink_delay:
	ldw x,#50			 		; delay(50ms)
	call delay
    jp mloop
msg_error:
	STRING "incorrect value!",$0a,$00
cmd_help:
	STRING "help",$00
cmd_mute
	STRING "mute",$00
cmd_unmute 
	STRING "unmute",00
msg_volume_max:
	STRING "volume is max",$0a,$00
msg_volume_min:
	STRING "volume is min",$0a,$00
msg_freq:
	STRING "freq=",$00
msg_volume:
	STRING "volume=",$00
msg_command:
    STRING "command: ",$00
msg_update:
	STRING "Read RDA5807m... ",$0a,$00
msg_on:
	STRING "Turn on",$0a,$00
msg_seekdown:
	STRING "Seek Down",$0a,$00
msg_seekup:
	STRING "Seek Up",$0a,$00
msg_mute:
	STRING "mute ON",$0a,$00
msg_unmute:
	STRING "mute OFF",$0a,$00
msg_help:
	STRING "Available commands:",$0A
    STRING "* s-/s+         - seek down/up with band wrap-around",$0A
    STRING "* v-/v+       - decrease/increase the volume",$0A
    STRING "* b-/b+       - bass on/off",$0A
    STRING "* d-/d+       - debug print on/off",$0A
    STRING "* mute/unmute - mute/unmute audio output",$0A
    STRING "* ww/jp/ws/es - cahnge band to: World Wide/Japan/West Europe/East Europe",$0A
    STRING "* 50MHz/65MHZ - cahnge low edge to 50MHz or 65MHz for East Europe band",$0A
    STRING "* c-/c+       - change space: 100kHz/200kHz/50kHz/25kHz",$0A
    STRING "* rst         - reset and turn off",$0A
    STRING "* on          - Turn On",$0A
    STRING "* ?f          - display currently tuned frequency",$0A
    STRING "* ?q          - display RSSI for current station",$0A
    STRING "* ?v          - display current volume",$0A
    STRING "* ?m          - display mode: mono or stereo",$0A
    STRING "* v=num       - set volume, where num is number from 0 to 15",$0A
    STRING "* t=num       - set SNR threshold, where num is number from 0 to 15",$0A
    STRING "* b=num       - set soft blend threshold, where num is number from 0 to 31",$0A
    STRING "* f=freq      - set frequency, e.g. f=103.8",$0A
    STRING "* ?|help      - display this list",$0A,$00,
    end

