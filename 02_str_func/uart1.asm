stm8/
	INTEL
    #include "STM8S103F.inc"
    segment 'rom'

    ; ----------- print uint8_t ------------------------
    ; input parameter: X
.uart1_print_num:
	pushw x
	pushw y
	push a
    ldw y,sp
    push #0
uart1_print_num_loop:
    ld a, #10
    div x,a
    add a,#30h
    push a
    tnzw x
    jrne uart1_print_num_loop
    ldw x,sp
    incw x
    call uart1_print_str
    ldw sp,y
	pop a
	popw y
	popw x
    ret

    ; ----------- print string -------------------------
    ;  input parameter:  X 
.uart1_print_str:
	pushw x
	push a
uart1_print_str_start
    ld a,(x)
    jreq uart1_str_exit
uart1_print_str_wait:
    btjf UART1_SR, #7, uart1_print_str_wait     ;wait if UART_DR is full yet (TXE == 0)
    ld UART1_DR, a 
    incw x
    jra uart1_print_str_start
uart1_str_exit:
	pop a
	popw x
    ret

    ; ----------- send char to UART1 -------------------
    ;  input parameter: A
.uart1_print_char:
    btjf UART1_SR, #7, uart1_print_char     ;wait if UART_DR is full yet (TXE == 0)
    ld UART1_DR, a
    ret
	end
	