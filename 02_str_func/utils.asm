stm8/
    segment 'rom'
	;----------- compare first two symbols from buffer --------------------------
    ; input parameter: buffer adr=0x0, symbols sp+4,sp+5
.check_cmd
	push a
	clrw x				; buffer address = 0x0
	ld a,($5,sp)
	cp a,(x)			;  check first symbol
	jrne  check_notOk
	incw x
	ld a,($4,sp)		
	cp a,(x)			; check second symbol
check_notOk:
	pop a
	popw x				; get return address
	addw sp,#2			; restore SP
	jp (x)				; ret

    ;----------- delay ---------------------------------
    ; input parameter: X - register
.delay:
	pushw x
	pushw y
delay_start
    ldw y, #4000      ; 1ms
delay_loop:
    subw y,#1
    jrne delay_loop
    decw x
    jrne delay_start
	popw y
	popw x
    ret
    end
	