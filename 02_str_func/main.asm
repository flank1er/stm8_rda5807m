stm8/
    #include "STM8S103F.inc"
	extern delay,uart1_print_str,uart1_print_num,uart1_print_char,strcmp,strnum
	extern strfrac,check_cmd

print_nl MACRO
	ld a, #$a              
    call uart1_print_char
	MEND
print_str MACRO msg
	ldw x, #msg
	call uart1_print_str
	MEND
	
LED equ 5
LEN	equ 10
EOL equ	LEN			; =Zero always
;-------- Variables ----------------------
STR	equ 0					; buffer[10bytes]
INDEX	cequ	{EOL+1}	; 1 byte
READY	cequ	{INDEX+1}	; 1 byte

    segment 'rom'
.main
    ;----------- Setup Clock ----------------------
    ; Setup fHSI = 16MHz
    clr CLK_CKDIVR
    ; Enable UART and turn off other  peripherals   
    mov CLK_PCKENR1, #0
    mov CLK_PCKENR2, #0
    bset CLK_PCKENR1, #3    ; enable UART1
    ;----------- Setup GPIO -----------------------
    bset PB_DDR, #LED       ; PB_DDR|=(1<<LED)
    bset PB_CR1, #LED       ; PB_CR1|=(1<<LED)
    ;----------- Setup UART1 ----------------------
    ; Clear
    clr UART1_CR1
    clr UART1_CR2
    clr UART1_CR3
    clr UART1_CR4
    clr UART1_CR5
    clr UART1_GTR
    clr UART1_PSCR
    ; Setup UART1, set 115200 Baud Rate
    bset UART1_CR1, #5      ; set UARTD, UART1 disable
    ; 9600 Baud Rate
    ;mov UART1_BRR2, #0x03
    ;mov UART1_BRR1, #0x68
    ; 115200 Baud Rate
    mov UART1_BRR2, #$0b
    mov UART1_BRR1, #$08   
    ; 230400 Baud Rate
    ;mov UART1_BRR2, #0x05
    ;mov UART1_BRR1, #0x04
    ; 921600 Baud Rate
    ;mov UART1_BRR2, #0x01
    ;mov UART1_BRR1, #0x01
    ; Trasmission Enable
	bset UART1_CR2, #3      ; set TEN, Transmission Enable
    bset UART1_CR2, #2      ; set REN, Receiver Enable
    bset UART1_CR2, #5      ; set RIEN, Enable Receiver Interrupt 
    ; enable UART1
    bres UART1_CR1, #5      ; clear UARTD, UART1 enable
    ;------------- End Setup ---------------------
	clr EOL					;set NULL/EOL
start:
    clr INDEX               ; INDEX=0
    clr READY               ; READY=0
    ; let's go...
    rim                     ; enable Interrupts
mloop:
    ; receive string
    btjt READY,#0,case
	jp blink
case:
	; CHECK: if was recived "mute" command
	clrw x
	ldw y,#cmd_mute
	call strcmp
	jrne unmute
	jp print_command
	; CHECK: if was recived "unmute" command
unmute:
	;clrw x
	ldw y,#cmd_unmute
	call strcmp
	jrne freq
	jp print_command
freq:
	; CHECK: if was recived "f=NUM.NUM" command
	push #'f'
	push #'='
	call check_cmd			; check
	jrne help				; if not "f=" then goto help
	ldw x,#02
	call strnum				; get integer part
	subw sp,#2
	ld ($1,sp),a			; (sp+1)=integer part
	call strfrac			; get fractional part
	ld ($2,sp),a			; (sp+2)=fractional part
	print_str msg_freq		; print "freq="
	ld a,($1,sp)
	clrw x
	ld xl,a					; x=interger part
	call uart1_print_num	; print x
	ld a, #'.'
    call uart1_print_char	; print dot
	ld a,($2,sp)
	ld xl,a					; x=fractional part
	call uart1_print_num	; print x
	print_nl				; NewLine
	addw sp,#2
help:
	; CHECK: if was recived "?" command
	clrw x					; if "v="
	ld a,(x)
	cp a,#'?'
	jrne help_2
	incw x
	ld a,(x)
	cp a,#0
	jrne help_2
	print_str msg_help
help_2:
	clrw x
	ldw y,#cmd_help
	call strcmp
	jrne volume
	print_str msg_help
volume:
	; CHECK: if was recived "v=NUM" command
	push #'v'
	push #'='
	call check_cmd			; check
	jrne next				; if not "v=" then goto next
	ldw x,#02
	call strnum				; convert string to number
	print_str msg_volume	; print "volume="
	clrw x
	ld xl,a					; x=strnum
	call uart1_print_num	; print x
	print_nl				; NewLine
next:
	jp start
	
print_command:
	print_str msg_command
	clrw x
	call uart1_print_str
	print_nl
	jp start
blink:
    bcpl PB_ODR, #LED       ; PB_ODR^=(1<<LED)
	ldw x,#500				; delay(500ms)
	call delay
    jp mloop

msg_freq:
	STRING "freq=",$00
msg_volume:
	STRING "volume=",$00
msg_command:
    STRING "command: ",$00
cmd_mute:
	STRING "mute",$00
cmd_help:
	STRING "help",$00
cmd_unmute:
	STRING "unmute",$00
msg_help:
	STRING "Available commands:",$0A
    STRING "* s-/s+         - seek down/up with band wrap-around",$0A
    STRING "* v-/v+       - decrease/increase the volume",$0A
    STRING "* b-/b+       - bass on/off",$0A
    STRING "* d-/d+       - debug print on/off",$0A
    STRING "* mute/unmute - mute/unmute audio output",$0A
    STRING "* ww/jp/ws/es - cahnge band to: World Wide/Japan/West Europe/East Europe",$0A
    STRING "* 50MHz/65MHZ - cahnge low edge to 50MHz or 65MHz for East Europe band",$0A
    STRING "* c-/c+       - change space: 100kHz/200kHz/50kHz/25kHz",$0A
    STRING "* rst         - reset and turn off",$0A
    STRING "* on          - Turn On",$0A
    STRING "* ?f          - display currently tuned frequency",$0A
    STRING "* ?q          - display RSSI for current station",$0A
    STRING "* ?v          - display current volume",$0A
    STRING "* ?m          - display mode: mono or stereo",$0A
    STRING "* v=num       - set volume, where num is number from 0 to 15",$0A
    STRING "* t=num       - set SNR threshold, where num is number from 0 to 15",$0A
    STRING "* b=num       - set soft blend threshold, where num is number from 0 to 31",$0A
    STRING "* f=freq      - set frequency, e.g. f=103.8",$0A
    STRING "* ?|help      - display this list",$0A,$00,
    end

