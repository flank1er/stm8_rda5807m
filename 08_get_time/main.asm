stm8/
    #include "STM8S103F.inc"
	extern delay,uart1_print_str,uart1_print_num,uart1_print_char,strcmp,strnum
	extern strfrac,check_cmd, uart1_print_str_nl
	extern rda5807m_update,rda5807m_control_write, rda5807m_write_register
	extern rda5807m_rds_update,rda5807m_get_readchan,uart1_print_hex
	extern calc_date
x=a MACRO
	clrw x
	ld xl,a
	MEND
y=a MACRO
	clrw y
	ld yl,a
	MEND
print_nl MACRO
	ld a, #$a              
    call uart1_print_char
	MEND
print_str MACRO msg
	ldw x, #msg
	call uart1_print_str
	MEND
print_str_nl MACRO msg
	ldw x, #msg
	call uart1_print_str_nl
	MEND
shiftX6 MACRO
	push a
	ld a,#6
	LOCAL shift
shift:
	sllw x
	dec a
	jrne shift
	pop a
	MEND
	
LED equ 5
LEN	equ 10
EOL equ	LEN					; =Zero always	; adr=0x0a
RDSTXT	equ $30
RDSTXT2A equ $40
;-------- Variables ----------------------
STR	equ 0					; buffer[10bytes]
INDEX	cequ	{EOL+1}		; 1 byte		; adr=0x0b
READY	cequ	{INDEX+1}	; 1 byte		; adr=0x0c
RDA_STAT cequ   {READY+1}	; 1 byte		; adr=0x0d
BAND	cequ	{RDA_STAT+1}; 1 byte		; adr=0x0e
SPACE	cequ	{BAND+1}	; 1 byte		; ard=0x0f
;-------- Constants ----------------------
RDA5807_CTRL equ $10
RDA5807_RDS cequ {RDA5807_CTRL+$10}
RDA5807M_SEQ_I2C_ADDRESS equ $20
RDA5807M_RND_I2C_ADDRESS equ $22
RDA5807M_CTRL_REG equ $02
RDA5807M_TUNER_REG equ $03
RDA5807M_CMD_RESET equ $0002
RDA5807M_RDS_H	equ RDA5807_RDS
;------------------------------------------

    segment 'rom'
.main
    ;----------- Setup Clock ----------------------
    ; Setup fHSI = 16MHz
    clr CLK_CKDIVR
    ; Enable UART and I2C,  turn off other  peripherals   
    ;mov CLK_PCKENR1, #0
    ;mov CLK_PCKENR2, #0
    ;bset CLK_PCKENR1, #3    ; enable UART1
	;bset CLK_PCKENR1, #0	; enable I2C
	;-------------------------------------------
	; for prevent power bounce
	ldw x,#500			 			; delay(500ms)
	call delay
    ;----------- Setup GPIO -----------------------
    ;bset PB_DDR, #LED       ; PB_DDR|=(1<<LED)
    ;bset PB_CR1, #LED       ; PB_CR1|=(1<<LED)
    ;----------- Setup UART1 ----------------------
    ; Clear
    clr UART1_CR1
    clr UART1_CR2
    clr UART1_CR3
    clr UART1_CR4
    clr UART1_CR5
    clr UART1_GTR
    clr UART1_PSCR
    ; Setup UART1, set 115200 Baud Rate
    bset UART1_CR1, #5      ; set UARTD, UART1 disable
    ; 9600 Baud Rate
    ;mov UART1_BRR2, #0x03
    ;mov UART1_BRR1, #0x68
    ; 115200 Baud Rate
    mov UART1_BRR2, #$0b
    mov UART1_BRR1, #$08   
    ; 230400 Baud Rate
    ;mov UART1_BRR2, #0x05
    ;mov UART1_BRR1, #0x04
    ; 921600 Baud Rate
    ;mov UART1_BRR2, #0x01
    ;mov UART1_BRR1, #0x01
    ; Trasmission Enable
	bset UART1_CR2, #3      ; set TEN, Transmission Enable
    bset UART1_CR2, #2      ; set REN, Receiver Enable
    bset UART1_CR2, #5      ; set RIEN, Enable Receiver Interrupt 
    ; enable UART1
    bres UART1_CR1, #5      ; clear UARTD, UART1 enable
	;------------- I2C Setup ----------------------
	bres I2C_CR1,#0			; PE=0, disable I2C before setup
	mov I2C_FREQR,#16		; peripheral frequency =16MHz
	clr I2C_CCRH			; =0
	mov I2C_CCRL,#80		; 100kHz for I2C
	bres I2C_CCRH,#7		; set standart mode(100�Hz)
	bres I2C_OARH,#7		; 7-bit address mode
	bset I2C_OARH,#6		; see reference manual
	;bset I2C_CR1,#0			; enable I2C
    ;------------- End Setup ---------------------
	clr EOL					;set NULL/EOL
	ldw x,#$40	
clear:
	clr (x)
	decw x
	jrne clear
	; filling by spaces
	call clr_rdstxt
	mov RDA_STAT,#$10
;---reset I2C Bus ---
	;bset I2C_CR1,#0
	;bres I2C_CR2,#7         ; set SWRST
	;bres I2C_CR1,#0
    ; let's go...
	mov IWDG_KR, #$cc;
    mov IWDG_KR, #$55;       	; unlock IWDG_PR & IWDG_RLR
    mov IWDG_PR, #6        		; =256
    mov IWDG_RLR,#$ff			; ~1sec for reset
	mov IWDG_KR, #$aa       	; lock IWDG_PR & IWDG_RLR
    rim                     	; enable Interrupts
start:
    clr INDEX               	; INDEX=0
    clr READY               	; READY=0
mloop:
    btjt READY,#0,mute			; if buffer not empty
	jp blink					; if buffer empty
mute:
	; CHECK: if was recived "mute" command
	clrw x						; ard of buffer: x=0
	ldw y,#cmd_mute
	call strcmp					; check incoming command
	jrne unmute	
	bres $10,#6					; clear DMUTE bit
	ldw x,$10					; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write	; write to REG_02
	addw sp,#2
	bset RDA_STAT,#0			; update
	print_str msg_mute			; print info message
	jp start					; break
unmute:
	; CHECK: if was recived "mute" command
	clrw x
	ldw y,#cmd_unmute
	call strcmp					; check incoming command
	jrne bass_on				; next
	bset $10,#6					; set DMUTE bit
	ldw x,$10					; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write ; write to REG_02
	addw sp,#2
	bset RDA_STAT,#0			; update
	print_str msg_unmute		; print info message
	jp start					; break
bass_on:
	push #'b'
	push #'+'
	call check_cmd				; check incoming command
	jrne bass_off				; next
	bset $10,#4					; set BASS flag
	ldw x,$10					; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write	; write to REG_02
	addw sp,#2
	bset RDA_STAT,#0			; update
	print_str msg_bass_on		; print info message
	jp start					; break
bass_off:
	push #'b'
	push #'-'
	call check_cmd				; check incoming command
	jrne mono					; next
	bres $10,#4					; reset BASS flag
	ldw x,$10					; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write	; write to REG_02
	addw sp,#2
	bset RDA_STAT,#0			; update
	print_str msg_bass_off		; print info message
	jp start					; break
mono:
	push #'?'
	push #'m'
	call check_cmd				; check incoming command
	jrne set_threshold			; next
	call print_stereo
	jp start					; break
set_threshold:
	push #'t'
	push #'='
	call check_cmd				; check incoming command
	jrne set_soft_blend			; if Z not set, then check next command
	print_str msg_threshold
	ld a,$16					; load high byte REG_4 to accumulator
	and a,#$f0					; clear bitfield [14:10]
	ld $16,a					; return value from accumulator
	ldw x,#02
	call strnum					; get input value
	and a,#$0f					; set mask for bitfield [4:0]
	x=a
	call uart1_print_num
	or a,$16					; add with high byte REG_5
	ld $16,a
	ldw x,$16					; 
	pushw x
	push #5
	call rda5807m_write_register; write REG_4
    addw sp,#03
	bset RDA_STAT,#0			; update
	print_nl
	jp start					; break
set_soft_blend:
	push #'b'
	push #'='
	call check_cmd				; check incoming command
	jrne band_ws				; if Z not set, then check next command
	print_str msg_soft_blend
	ld a,$1a					; load high byte REG_7 to accumulator
	and a,#$83					; clear bitfield [14:10]
	ld $1a,a					; return value from accumulator
	ldw x,#02
	call strnum					; get input value
	and a,#$1f					; set mask for bitfield [4:0]
	x=a
	call uart1_print_num
	sll a						; left  shift to two bits
	sll a
	or a,$1a					; add with high byte REG_7
	ld $1a,a
	ldw x,$1a
	pushw x
	push #7
	call rda5807m_write_register; write REG_7
    addw sp,#03
	bset RDA_STAT,#0			; update
	print_nl
	jp start					; break
band_ws:
	push #'w'
	push #'s'
	call check_cmd
	jrne band_50M
	print_str msg_change_band
	print_str msg_band_eu
	clr a
	jp set_band
band_50M:
	push #'5'
	push #'0'
	call check_cmd
	jrne band_ukv
	print_str msg_change_band
	print_str msg_band_50M
	btjf $1a,#1,omit_65M_50M
	bres $1a,#1
	ldw x,$1a
	pushw x
	push #7
	call rda5807m_write_register; write REG_7
    addw sp,#03
omit_65M_50M:
	ld a,#3
	jra set_band
band_ukv:
	push #'e'
	push #'s'
	call check_cmd
	jrne band_jp
	print_str msg_change_band
	print_str msg_band_ukv
	btjt $1a,#1,omit_50M_65M
	bset $1a,#1
	ldw x,$1a
	pushw x
	push #7
	call rda5807m_write_register; write REG_7
    addw sp,#03
omit_50M_65M:
	ld a,#3
	jra set_band
band_jp:
	push #'j'
	push #'p'
	call check_cmd
	jrne band_ww
	print_str msg_change_band
	print_str msg_band_jp
	ld a,#01
	jra set_band
band_ww:
	push #'w'
	push #'w'
	call check_cmd
	jrne debug_on	
	print_str msg_change_band
	print_str msg_band_ww
	ld a,#2
set_band:
	sll a
	sll a
	or a,SPACE
	or a,#$10					; set TUNE flag
	x=a
	pushw x
	push #03
    call rda5807m_write_register; write REG_3 /TUNE/
    addw sp,#03
	ldw x,#200			 			; delay(200ms)
	call delay
	jp seek_down_directly
debug_on:
	push #'d'
	push #'+'
	call check_cmd				; check incoming command
	jrne debug_off				; next
	print_str msg_debug_on		; print message: "Debug is ON"
	bset RDA_STAT,#3			; set debug flag
	jp start					; break
debug_off:
	push #'d'
	push #'-'
	call check_cmd				; check incoming command
	jrne raw_on				; next
	print_str msg_debug_off		; print message: "Debug is OFF"
	bres RDA_STAT,#3			; reset debug flag
	jp start					; break
raw_on:
	push #'r'
	push #'+'
	call check_cmd				; check incoming command
	jrne raw_off				; next
	print_str msg_raw_rds_log_on	; print message: "Debug is ON"
	bset RDA_STAT,#5			; set debug flag
	jp start					; break
raw_off:
	push #'r'
	push #'-'
	call check_cmd				; check incoming command
	jrne log_on				; next
	print_str msg_raw_rds_log_off	; print message: "Debug is OFF"
	bres RDA_STAT,#5			; reset debug flag
	jp start					; break
log_on:
	push #'l'
	push #'+'
	call check_cmd				; check incoming command
	jrne log_off				; next
	print_str msg_rds_log_on	; print message: "Debug is ON"
	bset RDA_STAT,#5			; set debug flag
	bset RDA_STAT,#6			; set log flag
	jp start					; break
log_off:
	push #'l'
	push #'-'
	call check_cmd				; check incoming command
	jrne space_down				; next
	print_str msg_rds_log_off	; print message: "Debug is OFF"
	bres RDA_STAT,#5			; reset debug flag
	bres RDA_STAT,#6			; reset log flag
	jp start					; break
space_down:
	push #'c'
	push #'-'
	call check_cmd				; check incoming command
	jrne space_up				; if Z not set, then check next command
	ld a,SPACE
	dec a
	and a,#3
	push a
;-- CASE (space)
	sll a	
	x=a
	ldw x,(case_recalc2,x)
	jp (x)	
case_recalc2:
	DC.W space2_100,space2_200,space2_50,space2_25
space2_100:
	print_str msg_space100
	call rda5807m_get_readchan
	sllw x
	jra space_recalc
space2_200:
	print_str msg_space200
	call rda5807m_get_readchan
	srlw x
	srlw x
	jra space_recalc
space2_50:
	print_str msg_space50
	call rda5807m_get_readchan
	srlw x
	jra space_recalc
space2_25:
	print_str msg_space25
	call rda5807m_get_readchan
	sllw x
	sllw x
	jra space_recalc
space_up:
	push #'c'
	push #'+'
	call check_cmd				; check incoming command
	jrne set_vol				; if Z not set, then check next command
	ld a,SPACE
	inc a
	and a,#3
	push a
;-- CASE (space)
	sll a	
	x=a
	ldw x,(case_recalc,x)
	jp (x)	
case_recalc:
	DC.W space_100,space_200,space_50,space_25
space_100:
	print_str msg_space100
	call rda5807m_get_readchan
	srlw x
	srlw x
	jra space_recalc
space_25:
	print_str msg_space25
	call rda5807m_get_readchan
	sllw x
	jra space_recalc
space_50:
	print_str msg_space50
	call rda5807m_get_readchan
	sllw x
	sllw x
	jra space_recalc
space_200:
	print_str msg_space200
	call rda5807m_get_readchan
	srlw x
space_recalc:
	shiftX6
	ld a,BAND
	cp a,#4
	jrne not_50M_band
	ld a,#3
not_50M_band:
	sll a
	sll a
	or a,(1,sp)
	pushw x
	or a,(2,sp)
	or a,#$10					; set TUNE flag
	ld xl,a
	addw sp,#03
	pushw x
	push #03
    call rda5807m_write_register; write REG_3 /TUNE/
    addw sp,#03
    bset RDA_STAT,#0            ; to update
    bset RDA_STAT,#1            ; print freq
	jp start					; break
set_vol:
	push #'v'
	push #'='
	call check_cmd				; check incoming command
	jrne vol_down				; if Z not set, then check next command
	ldw x,#02
	call strnum					; get integer part
	and a,#$0f					; mask parameter
	ldw x,$16					; x=REG_5 /VOLUME/
	push a
	ld a,xl
	and a,#$f0					; x=(x & 0xfff0)
	or a,(1,sp)					; x=(x | num)
	ld xl,a						
	pop a
	pushw x
	push #05					; select REG_5 to write
	call rda5807m_write_register; write volume to REG_5 /VOLUME/
	addw sp,#03
	bset RDA_STAT,#0			; update
	jp start					; break
vol_down:
	push #'v'
	push #'-'
	call check_cmd				; check incoming command
	jrne vol_up					; next
	ld a,$17					; a=ram[0x17] (low byte REG_05 /VOLUME/)
	and a,#$0f					; mask
	jreq vol_min				; if current volume is minimum(=0)
	;------------
	print_str msg_volume		; print info message  "volume="
	clrw x						; x=0
	dec a						; volume -=1
	ld xl,a						; x=a
	call uart1_print_num		; print volume
	print_nl					; print NL
	;----------------
	ldw x,$16					; x=REG_05 /VOLUME/
	decw x						; volume down
	pushw x
	push #05
	call rda5807m_write_register; write X to REG_05 /VOLUME/
	addw sp,#03
	bset RDA_STAT,#0			; update
	jp start					; break
vol_min:
	print_str msg_volume_min	; print error message
	jp start
vol_up:
	push #'v'
	push #'+'
	call check_cmd				; check incoming command
	jrne seek_down				; next
	ld a,$17					; a=ram[0x17] (low byte REG_05 /VOLUME/)
	and a,#$0f					; mask
	cp a,#$0f					; if (a==15)
	jreq vol_max				; if current volume is maximum(=15)
	;------------
	print_str msg_volume		; print info message "volume="
	clrw x
	inc a
	ld xl,a	
	call uart1_print_num		; print volume
	print_nl					; print NL
	;----------------
	ldw x,$16					; x=REG_05 /VOLUME/
	incw x						; vlume up
	pushw x
	push #05
	call rda5807m_write_register; write X to REG_05 /VOLUME/
	addw sp,#03
	bset RDA_STAT,#0			; set "to update" flag
	jp start					; break
vol_max:
	print_str msg_volume_max	; print error message
	jp start
seek_down:
	push #'s'
	push #'-'
	call check_cmd				; check incoming command
	jrne seek_up				; next 
seek_down_directly:
	bres $10,#1					; seek-down
	bset $10,#0					; seek enable
	ldw x,$10					; X = ram[0x10]
	pushw x
	call rda5807m_control_write ; write X to REG_02 /CONTROL/
	addw sp,#2
	bset RDA_STAT,#0			; set "to update" flag
	bset RDA_STAT,#1			; print freq
	print_str msg_seekdown		; print info message
	jp start					; break
seek_up:
	push #'s'
	push #'+'
	call check_cmd				; check incoming command
	jrne rssi					; next
	bset $10,#1					; seek-up
	bset $10,#0					; seek enable
	ldw x,$10					; X = ram[0x10]
	pushw x
	call rda5807m_control_write ; write X to REG_02 /CONTROL/
	addw sp,#2
	bset RDA_STAT,#0			; set "to update" flag
	bset RDA_STAT,#1			; print freq
	print_str msg_seekup		; print info message
	jp start					; break
rssi:
	push #'?'
	push #'q'
	call check_cmd				; check incoming command
	jrne on_cmd					; next
	bset RDA_STAT,#2
	jp print_rssi				; goto print_rssi
on_cmd:
	push #'o'
	push #'n'
	call check_cmd				; check incoming command
	jrne rst					; next
	print_str msg_on			; print info message
	ldw x,$10
	ld a,xl
	or a,#RDA5807M_CMD_RESET    ; set RESET bit
	ld xl,a
	pushw x
	call rda5807m_control_write ; write to REG_02 /CONTROL/
	addw sp,#2
	ldw x,#$c10d				; REG_02=0xC10D (Turn_ON + SEEK)
	pushw x
	call rda5807m_control_write ; write to REG_02 /CONTROL/
	addw sp,#2
	bset RDA_STAT,#0			; update
	bset RDA_STAT,#1			; print freq
	jp start					; check incoming command
rst:
	clrw x
	ldw y,#cmd_rst
	call strcmp					; check incoming command
	jrne freq					; next
	bset $11,#1					; set RESET flag
	ldw x,$10					; load CONTROL reg to X
	pushw x
	call rda5807m_control_write ; write X to REG_02 /CONTROL/
	addw sp,#2
	
	;print_str msg_reset			; print message "Reset"
	;mov RDA_STAT,#$10
	mov IWDG_KR, #$cc;
    mov IWDG_KR, #$55;       	; unlock IWDG_PR & IWDG_RLR
    mov IWDG_PR, #6        		; =256
    mov IWDG_RLR, #1
	mov IWDG_KR, #$aa       	; lock IWDG_PR & IWDG_RLR
rst_loop:
	jra rst_loop
	;jp start					; break
freq:
	; CHECK: if was recived "f=NUM.NUM" command
	push #'f'
	push #'='
	call check_cmd				; check
	jrne help					; if not "f=" then goto help
	ldw x,#02
	call strnum					; get integer part
	y=a							; y = integer part
	call strfrac				; get fractional part
	push a						; fractional part to stack
	ld a,BAND
	sll a
	x=a
	ldw  x,(band_range,x)		; x=low edge of current band
	pushw x
	subw y,(1,sp)				; y = (integer part - low edge of current band)
	ld a,SPACE
	x=a
	ld a,(spaces,x)				; load scaler
	mul y,a						; y = y * scaler
	ld a,(3,sp)
	x=a							; x = fractional part
	pushw y						; save y
	ld a,SPACE
	sll a
	y=a
	ldw y,(case_freq,y)
	jp (y)
case_freq: 
	DC.W sp_is_0, sp_is_1, sp_is_2, sp_is_3
sp_is_3
	sllw x
	incw x
	ld a,#5
	div x,a
	jra sp_is_0	
sp_is_2:
	ld a,#5
	div x,a						; x=x/5
	jra sp_is_0
sp_is_1:
	srlw x
sp_is_0:
	addw x,(1,sp)				; x=x+y
	addw sp,#5
	push SPACE
	jp space_recalc
help:
	; CHECK: if was recived "?" command
	clrw x						; if "?"
	ld a,(x)
	cp a,#'?'					; check incoming command
	jrne help_2
	incw x
	ld a,(x)
	cp a,#0						; NULL 
	jrne help_2
	print_str msg_help			; print help message
	jp start					; break
help_2:
	clrw x
	ldw y,#cmd_help				
	call strcmp					; check incoming command
	jrne volume
	print_str msg_help			; print help message
	jp start					; break
volume:
	; get current Gain Control Bits(volume)
	push #'?'
	push #'v'
	call check_cmd				; check incoming command
	jrne get_freq				; if not "?v" then goto next
	print_str msg_volume		; print "volume="
	ld a,$17					; load low byte of REG_5 /VOLUME/
	and a,#$0f					; mask
	clrw x
	ld xl,a						; x = a
	call uart1_print_num		; print volume
	print_nl					; print NewLine
	jp start					; break
get_freq:
	; get current frequency
	push #'?'
	push #'f'
	call check_cmd				; check incoming command
	jrne next					; if not "?v" then goto next
	bset RDA_STAT,#0			; update
	bset RDA_STAT,#1			; print freq
next:
	; ----- END OF CASE ---------------
	jp start
	
print_command:
	print_str msg_command
	clrw x
	call uart1_print_str_nl			; print incoming string
	jp start
;-- READ BLOCK --------------------------
;   read from RDA5807m registers
;-----------------------------------------
blink:
	bset RDA_STAT,#3				; uncomment for DEBUG
	btjt RDA_STAT,#4, update_rds
	btjt RDA_STAT,#0, update_rds 	; if need read from RDA5807m, then goto update_rds
	btjf RDA_STAT,#5, no_rds_log
	call rda5807m_rds_update
	call log_rds
no_rds_log:
	jp blink_delay					; else goto blink
update_rds:
	btjf RDA_STAT,#3,omit_msg_update_rds
	print_str msg_update_rds		; print message: "Read RDS Registers"
omit_msg_update_rds:
	call rda5807m_rds_update		; read RDS block reg[0x0a - 0x0f]
	tnz a							
	jreq update_rda_reg				; if not failed, then goto read 
	jp blink_delay					; else goto blink
update_rda_reg:
	btjt RDA_STAT,#4,omit_msg_update_ctl
	btjf RDA5807M_RDS_H,#6, seek 	; if Seek or Tune not Complete	yet
	btjf RDA_STAT,#3,omit_msg_update_ctl
	print_str msg_update_ctl		; print message: "Read Control Registers"
omit_msg_update_ctl:
	call rda5807m_update			; read CONTROL block of rda5807m reg[0x02 - 0x07]
	btjf RDA_STAT,#3,omit_msg_update_complete
	print_str msg_update_complete	; print message "Read Registers was Complete""=
omit_msg_update_complete:
	btjt RDA_STAT,#4,first_loop_end
	jra store_band_and_space		; if read was success
	;tnz a
	;jreq store_band_and_space		; if read was success
seek:
	jp blink_delay					; else goto blink
first_loop_end:
	bres RDA_STAT,#4
	print_str msg_ready
;---PRINT FREQUENCY of CURRENT STATION -----
store_band_and_space:
	bres RDA_STAT,#0				; if success, then 1) reset flag
;-- write current space and band ---------	
	ld a,$13						; get low byte REG_03 /TUNER/
	and a, #$03						; mask bitfield [1:0]
	ld SPACE,a						; store SPACE value
	ld a,$13						; get low byte REG_03 /TUNER/
	and a,#$0c						; mask bitfield [3:2]
	srl a							; (a>>1), right logical shift
	srl a							; (a>>1), right logical shift
	cp a,#3
	jrne leave_50M
	btjt $1a,#1,leave_50M
	ld a,#4
leave_50M:
	ld BAND,a						; store BAND value
	btjt RDA_STAT,#1,print_freq		; if print of station frequency and rssi
	jp blink_delay					; else goto blink
print_freq:
	call rda5807m_get_readchan
	ld a,SPACE
	y=a 
	ld a,(spaces,y)					; load current scaler
	y=a								; load scaler to Y reg
	divw x,y						; X = X / Scaler
	pushw x
	ld a,SPACE
	sll a
	x=a
	ldw x,(chose_space,x)
	jp (x)	
chose_space:
	DC.W sp_0,sp_1,sp_2,sp_3
sp_1:
	sllw y
	jra sp_0
sp_2:
	ld a,#5
	mul y,a
	jra sp_0
sp_3:
	ld a,#25
	mul y,a
sp_0:
	print_str msg_freq				; print X
	ld a,BAND
	sll a
	x=a
	ldw x,(band_range,x)
	addw x,(1,sp)
	addw sp,#2
	;popw x
	;addw x,band_range				; X = X + low range of current band
	call uart1_print_num
	ld a,#'.'
	call uart1_print_char			; print dot
	ldw x,y
	ld a,#3
	cp a,SPACE
	jrne print_fract
	cpw x,#100
	jrsge print_fract
	ld a,#'0'
	call uart1_print_char
print_fract:
	call uart1_print_num			; print fractional part of frequency
	print_str msg_mhz
	btjf RDA_STAT,#3, print_rssi
;---print space --------
	print_str msg_space
	clrw x
	ld a,SPACE
	ld xl,a
	call uart1_print_num
	print_nl
;---print band ---------
	print_str msg_band
	clrw x
	ld a,BAND
	ld xl,a
	call uart1_print_num
	print_nl
;---print chan value ----
	print_str msg_chan
	call rda5807m_get_readchan
	call uart1_print_num
	print_nl
;---print stereo mode ----
	call print_stereo
;---print rssi ---------
print_rssi:							; print rssi
	ld a,$22						; load high byte 0x0B reg to accumulator
	srl a							; a = (a >> 1)
	print_str msg_rssi
	clrw x
	ld xl,a
	pushw x
	call uart1_print_num			; print rssi
	popw x
	print_str msg_dbuv
	bres RDA_STAT,#1
	btjt RDA_STAT,#2,rssi_quit
blink_delay:
	btjf RDA_STAT,#5,delay_250
	;ldw x,#87
	ldw x,#43
	jra delay_loop
delay_250:
	ldw x,#250			 			; delay(250ms)
delay_loop:
	mov IWDG_KR, #$aa  				; reset watchdog
	call delay
    jp mloop
rssi_quit
	clr RDA_STAT
	jp start
	; ---- subreoutines ------------------
print_stereo:
	btjt $20,#2,stereo			; check ST flag of 0x0a register
	print_str msg_mono			; print info message
	ret
stereo:
	print_str msg_stereo		; print info message
	ret
log_rds:
	pushw x
	pushw y
	push a
	btjf RDA_STAT,#6, log_rds_raw
	ld a,$26
	and a,#$f8
	cp a,#$40				; 0x4A
	jreq log_rds_4a
	cp a,#$0				; 0x0A
	jrne switch_rds_00
	jp log_rds_0a
switch_rds_00:
	cp a,#$08				; 0x0B
	jrne switch_rds_01
	jp log_rds_0a
switch_rds_01:
	cp a,#$20				; 0x2A
	jreq log_rds_2a
	jp end_rds_log
;log_rds_0a:
	;push #$0a
	;jra log_rds_raw
;	jra log_rds_0a
;log_rds_2a:
	;push #$2a
	;jra log_rds_raw
	;print_str msg_radiotext
;	jp log_rds_2a
	jp end_rds_log

log_rds_4a:
	;push #$4a
log_rds_raw:
	ldw y,#$20
log_rds_loop:
	ldw x,y
	ldw x,(x)
	call uart1_print_hex
	ld a, #' '              
	call uart1_print_char
	addw y,#2
	cpw y,#$2c
	jrne log_rds_loop
	btjt RDA_STAT,#6, log_rds_print_time
	jp end_raw_log
log_rds_print_time:
	jp log_rds_print_4a
;	pop a
;	cp a,#$4a
;	jreq log_rds_print_4a
;	cp a,#$0a
;	jreq log_rds_print_0a
;	jp end_raw_log
log_rds_2a:
	ld a,$27
	and a,#$0f
	tnz a
	jrne log_2a_idx_not_zero
	tnz $3f
	jreq log_2a_idx_zero
	print_str msg_rdx
	ldw x,#RDSTXT2A
	call uart1_print_str
	print_nl
	jra log_2a_idx_zero
log_2a_wrong:
	call clr_rdstxt_2a
	jp end_rds_log
log_2a_idx_not_zero:
	push a
	sub a,$3f
	cp a,#2
	pop a
	jrnc log_2a_wrong
	ld $3f,a
	sll a
	sll a
	jra jump_01
log_2a_idx_zero:
	call clr_rdstxt_2a
jump_01
	add a,#RDSTXT2A
	y=a
	ldw x,$28
	ldw (y),x
	ldw x,$2a
	ldw (2,y),x
	clr (4,y)
	ld a,$27
	and a,#$0f
	cp a,#$f
	jreq log_2a_print_radiotext
	jp end_rds_log
log_2a_print_radiotext:
	print_str msg_rdx
	ldw x,#RDSTXT2A
	call uart1_print_str
	clr $3f
	jp end_raw_log
log_rds_0a:
;log_rds_print_0a:
	ld a,$27			; get index
	and a,#$3
	tnz a
	jrne log_rda_idx_not_zero
	call clr_rdstxt
	jra log_rda_idx_zero
log_rds_wrong:
	call clr_rdstxt
	jp end_rds_log
log_rda_idx_not_zero:
	push a
	sub a,$3e
	cp a,#2
	pop a
	jrnc log_rds_wrong
	ld $3e,a
	sll a
log_rda_idx_zero:
	add a,#RDSTXT
	y=a
	ldw x,$2a
	ldw (y),x
	ld a,$27			; get index
	and a,#$3
	cp a,#3
	jreq log_rda_idx_print
	jp end_rds_log
log_rda_idx_print:
	ldw x,#RDSTXT
	call uart1_print_str
	clr $3e
error_rds:
	jp end_raw_log
log_rds_print_4a:
	print_str msg_time
	; extract Hour
	ldw x,$29
	srlw x
	ld a,xl
	srl a
	srl a
	srl a
	x=a
	call uart1_print_num
	ld a,#':'
	call uart1_print_char
	; extract Minutes
	ldw x, $2a
	sllw x
	sllw x
	ld a,xh
	and a,#$3f
	x=a
	call uart1_print_num
	print_str msg_offset
	btjf $2b,#5,negative_offset
	ld a,#'+'
	call uart1_print_char
	jra log_rds_print_offset
negative_offset:
	ld a,#'+'
	call uart1_print_char
log_rds_print_offset:
	ld a,$2b
	and a,#$1f
	x=a
	call uart1_print_num
	ldw x,$28
	srlw x
	btjt $27,#1,log_rds_error_date
	ld a,$27
	and a,#$03
	swap a
	sll a
	sll a
	sll a
	jrc log_rds_error_date
	push a
	ld a,xh
	or a,(1,sp)
	ld xh,a
	pop a
	cpw x,#$e661				; 8 may 2020
	jrc log_rds_error_date
	pushw x
	print_str msg_is_date
	;pushw x
	clrw x
	pushw x
	call calc_date
	pop a
	x=a
	call uart1_print_num
	ld a, #'-'
    call uart1_print_char
	pop a
	x=a
	call uart1_print_num
	ld a, #'-'
    call uart1_print_char
	pop a
	x=a
	addw x,#1900
	call uart1_print_num
	ld a, #' '
    call uart1_print_char
	pop a
	inc a
	x=a
	call uart1_print_num
	jra end_raw_log
log_rds_error_date:
	print_str msg_error_date
end_raw_log:	
	print_nl
end_rds_log:
	pop a
	popw y
	popw x
	ret
clr_rdstxt:
	pushw x
	push a
	ldw x, #{RDSTXT+8}
	ld a,#$20
fill:
	decw x				
	ld (x),a
	cpw x,#RDSTXT
	jrne fill
	clr $3e
	pop a
	popw x
	ret
clr_rdstxt_2a:
	pushw x
	push a
	ldw x, #{RDSTXT2A+64}
	ld a,#$20
fill_2a:
	decw x				
	ld (x),a
	cpw x,#RDSTXT2A
	jrne fill_2a
	clr $3f
	pop a
	popw x
	ret
band_range:
	DC.W 87,76,76,65,50
spaces:
	DC.B 10,5,20,40
cmd_rst:
	STRING "rst",$00
cmd_help:
	STRING "help",$00
cmd_mute
	STRING "mute",$00
cmd_unmute 
	STRING "unmute",$00
msg_offset
	STRING ", offset: ",$00
msg_time
	STRING "time: ",$00
msg_soft_blend:
	STRING "soft blend threshold = ",$00
msg_threshold
	STRING "Threshold = ",$00
msg_error:
	STRING "incorrect value!",$0a,$00
msg_ready:
	STRING "RDA5807m is Ready. Enter '?' for help",$0a,$00
msg_change_band:
	STRING "Change band to ",$00
msg_band_ww:
	STRING "World-Wide Band (76-108MHz)",$0a,$00
msg_band_jp:
	STRING "Japan Band (76-91MHz)",$0a,$00
msg_band_ukv:
	STRING "exUSSR Band (65-76MHz)",$0a,$00
msg_band_50M:
	STRING "50MHz Band (50-65MHz)",$0a,$00
msg_band_eu:
	STRING "West Band (87-108MHz)",$0a,$00
msg_debug_on:
	STRING "Debug is ON",$0a,$00
msg_debug_off:
	STRING "Debug is OFF",$0a,$00
msg_raw_rds_log_on:
	STRING "RAW RDS logging is ON",$0a,$00
msg_raw_rds_log_off:
	STRING "RAW RDS logging is OFF",$0a,$00
msg_rds_log_on:
	STRING "RDS logging is ON",$0a,$00
msg_rds_log_off:
	STRING "RDS logging is OFF",$0a,$00
msg_chan:
	STRING "chan: ",$00
msg_reset:
	STRING "Reset",$0a,$00
msg_space200:
	STRING "Change space to 200kHz",$0a,$00
msg_space100:
	STRING "Change space to 100kHz",$0a,$00
msg_space50:
	STRING "Change space to 50kHz",$0a,$00
msg_space25:
	STRING "Change space to 25kHz",$0a,$00
msg_volume_max:
	STRING "volume is max",$0a,$00
msg_volume_min:
	STRING "volume is min",$0a,$00
msg_space:
	STRING "Space: ",$00
msg_band:
	STRING "Band: ",$00
msg_rssi:
	STRING "rssi: ",$00
msg_dbuv:
	STRING " dBuV",$0a,$00
msg_mhz:
	STRING " MHz",$0a,$00
msg_freq:
	STRING "freq=",$00
msg_volume:
	STRING "volume=",$00
msg_command:
    STRING "command: ",$00
msg_update_rds:
	STRING "Read RDS Registers",$0a,$00
msg_update_ctl:
	STRING "Read Control Registers",$0a,$00
msg_update_complete:
	STRING "Read Registers was Complete",$0a,$00
msg_on:
	STRING "Turn on",$0a,$00
msg_seekdown:
	STRING "Seek Down",$0a,$00
msg_seekup:
	STRING "Seek Up",$0a,$00
msg_mute:
	STRING "mute ON",$0a,$00
msg_unmute:
	STRING "mute OFF",$0a,$00
msg_bass_on:
	STRING "Bass On",$0a,$00
msg_bass_off:
	STRING "Bass Off",$0a,$00
msg_stereo:
	STRING "Stereo Mode",$0a,$00
msg_mono:
	STRING "Mono Mode",$0a,$00
msg_error_date:
	STRING " Incorrect Date", $0a,$00
msg_is_date:
	STRING " Date: ",$00
msg_rdx:
	STRING "RDX: ",$00
msg_radiotext:
	STRING "RADIOTEXT: ", $0a,$00
msg_help:
	STRING "Available commands:",$0A
    STRING "* s-/s+         - seek down/up with band wrap-around",$0A
    STRING "* v-/v+       - decrease/increase the volume",$0A
    STRING "* b-/b+       - bass on/off",$0A
    STRING "* d-/d+       - debug print on/off",$0A
	STRING "* r-/r+       - print  RDS raw log on/off",$0A
	STRING "* l-/l+       - print RDS messages on/off",$0A
    STRING "* mute/unmute - mute/unmute audio output",$0A
    STRING "* ww/jp/ws/es/50 - cahnge band to: World Wide/Japan/West Europe/East Europe/50MHz",$0A
    STRING "* c-/c+       - change space: 100kHz/200kHz/50kHz/25kHz",$0A
    STRING "* rst         - reset and turn off",$0A
    STRING "* on          - Turn On",$0A
    STRING "* ?f          - display currently tuned frequency",$0A
    STRING "* ?q          - display RSSI for current station",$0A
    STRING "* ?v          - display current volume",$0A
    STRING "* ?m          - display mode: mono or stereo",$0A
    STRING "* v=num       - set volume, where num is number from 0 to 15",$0A
    STRING "* t=num       - set SNR threshold, where num is number from 0 to 15",$0A
    STRING "* b=num       - set soft blend threshold, where num is number from 0 to 31",$0A
    STRING "* f=freq      - set frequency, e.g. f=103.8",$0A
    STRING "* ?|help      - display this list",$0A,$00,
    end

