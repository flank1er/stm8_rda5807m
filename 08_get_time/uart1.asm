stm8/
	INTEL
    #include "STM8S103F.inc"
    segment 'rom'

    ; ----------- print hex number----------------------
    ; input parameter: X register
.uart1_print_hex:
	pushw x
	pushw y
	push a
	ldw y, sp
	ldw 1ch,y
	clrw y
	push #0
hex_loop:
	ld a,#10h
	div x,a
	ld yl,a
	ld a,(hex_digit,y)
	push a
	tnzw x
	jrne hex_loop
	push #'x'
	push #'0'
    ldw x,sp
    incw x
    call uart1_print_str
	ldw y,1ch
	ldw sp,y
	pop a
	popw y
	popw x
	ret	
hex_digit:
	STRING "0123456789ABCDEF"

    ; ----------- print uint8_t ------------------------
    ; input parameter: X
.uart1_print_num:
	pushw x
	pushw y
	push a
    ldw y,sp
    push #0
uart1_print_num_loop:
    ld a, #10
    div x,a
    add a,#30h
    push a
    tnzw x
    jrne uart1_print_num_loop
    ldw x,sp
    incw x
    call uart1_print_str
    ldw sp,y
	pop a
	popw y
	popw x
    ret

    ; ----------- print string -------------------------
    ;  input parameter:  X 
.uart1_print_str:
	pushw x
	push a
uart1_print_str_start
    ld a,(x)
    jreq uart1_str_exit
uart1_print_str_wait:
    btjf UART1_SR, #7, uart1_print_str_wait     ;wait if UART_DR is full yet (TXE == 0)
    ld UART1_DR, a 
    incw x
    jra uart1_print_str_start
uart1_str_exit:
	pop a
	popw x
    ret

	; ----------- print string with NL -------------------------
    ;  input parameter:  X 
.uart1_print_str_nl:
	pushw x
	push a
uart1_print_str_nl_start
    ld a,(x)
    jreq uart1_str_nl_exit
uart1_print_str_nl_wait:
    btjf UART1_SR, #7, uart1_print_str_nl_wait     ;wait if UART_DR is full yet (TXE == 0)
    ld UART1_DR, a 
    incw x
    jra uart1_print_str_nl_start
uart1_str_nl_exit:
	ld a, #0ah
uart1_print_str_nl_check:
	btjf UART1_SR, #7, uart1_print_str_nl_check     ;wait if UART_DR is full yet (TXE == 0)
    ld UART1_DR, a
	pop a
	popw x
    ret

    ; ----------- send char to UART1 -------------------
    ;  input parameter: A
.uart1_print_char:
    btjf UART1_SR, #7, uart1_print_char     ;wait if UART_DR is full yet (TXE == 0)
    ld UART1_DR, a
    ret
	end
	