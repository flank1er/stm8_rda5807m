stm8/
    #include "STM8S103F.inc"
	#include "rda5807.inc"
	extern strfrac, get_state, chose_state, band_range, spaces
	extern rda5807m_rds_update,rda5807m_get_readchan, rda5807m_update
	extern delay, log_rds, clr_rdstxt_2a, clr_rdstxt,  strcmp, strnum, print_freq, print_rssi
	#IF USE_UART
	extern uart1_print_str,uart1_print_num,uart1_print_char, uart1_print_str_nl, uart1_print_hex
	#ENDIF

RDS_REG_UPDATE	equ 0
CTRL_REG_UPDATE	equ 1
CHECK_TUNE		equ 2
READY_MSG		equ 3
IRQ_ENABLE		equ 4
FREQ_PRINT		equ 5
RSSI_PRINT		equ 6
SPACE_BAND		equ 7

    segment 'rom'
.main:
    ;----------- Setup Clock ----------------------
    ; Setup fHSI = 16MHz
    clr CLK_CKDIVR
    ; Enable UART and I2C,  turn off other  peripherals   
    ;mov CLK_PCKENR1, #0
    ;mov CLK_PCKENR2, #0
    ;bset CLK_PCKENR1, #3    ; enable UART1
	;bset CLK_PCKENR1, #0	; enable I2C
	;-------------------------------------------
	; for prevent power bounce
	ldw x,#500			 			; delay(500ms)
	call delay
    ;----------- Setup GPIO -----------------------
    ;bset PB_DDR, #LED       ; PB_DDR|=(1<<LED)
    ;bset PB_CR1, #LED       ; PB_CR1|=(1<<LED)
	#IF USE_UART
    ;----------- Setup UART1 ----------------------
    ; Clear
    clr UART1_CR1
    clr UART1_CR2
    clr UART1_CR3
    clr UART1_CR4
    clr UART1_CR5
    clr UART1_GTR
    clr UART1_PSCR
    ; Setup UART1, set 115200 Baud Rate
    bset UART1_CR1, #5      ; set UARTD, UART1 disable
    ; 9600 Baud Rate
    ;mov UART1_BRR2, #0x03
    ;mov UART1_BRR1, #0x68
    ; 115200 Baud Rate
    mov UART1_BRR2, #$0b
    mov UART1_BRR1, #$08   
    ; 230400 Baud Rate
    ;mov UART1_BRR2, #0x05
    ;mov UART1_BRR1, #0x04
    ; 921600 Baud Rate
    ;mov UART1_BRR2, #0x01
    ;mov UART1_BRR1, #0x01
    ; Trasmission Enable
	bset UART1_CR2, #3      ; set TEN, Transmission Enable
    bset UART1_CR2, #2      ; set REN, Receiver Enable
    bset UART1_CR2, #5      ; set RIEN, Enable Receiver Interrupt 
    ; enable UART1
    bres UART1_CR1, #5      ; clear UARTD, UART1 enable
	#ENDIF
	;------------- I2C Setup ----------------------
	bres I2C_CR1,#0			; PE=0, disable I2C before setup
	mov I2C_FREQR,#16		; peripheral frequency =16MHz
	clr I2C_CCRH			; =0
	mov I2C_CCRL,#80		; 100kHz for I2C
	bres I2C_CCRH,#7		; set standart mode(100�Hz)
	bres I2C_OARH,#7		; 7-bit address mode
	bset I2C_OARH,#6		; see reference manual
	;bset I2C_CR1,#0			; enable I2C
    ;------------- End Setup ---------------------
	clr EOL					;set NULL/EOL
	ldw x,#$3f	
clear:
	clr (x)
	decw x
	jrpl clear
	; filling by spaces
	call clr_rdstxt
;---reset I2C Bus ---
	;bset I2C_CR1,#0
	;bres I2C_CR2,#7         ; set SWRST
	;bres I2C_CR1,#0
    ; let's go...
	#IF WDOG
	mov IWDG_KR, #$cc;
    mov IWDG_KR, #$55;       	; unlock IWDG_PR & IWDG_RLR
    mov IWDG_PR, #6        		; =256
    mov IWDG_RLR,#$ff			; ~1sec for reset
	mov IWDG_KR, #$aa       	; lock IWDG_PR & IWDG_RLR
	#ENDIF
	#IF USE_UART
	mov RDA_STAT,#$04			; set DEBUG flag
	#ENDIF
	push #$ff					; end of states
	push #IRQ_ENABLE			; enable interrupt
	push #READY_MSG				; print message "get ready"
	push #CTRL_REG_UPDATE		; update control registers
	push #RDS_REG_UPDATE		; update RDS registers
start:
    btjf READY,#0,loop			; if buffer not empty
	call get_state
	tnz a
	jrmi start
	sll a
	x=a
	ldw x,(chose_state,x)
	call (x)
	clr INDEX               	; INDEX=0
    clr READY               	; READY=0
	#IF USE_UART
	btjf RDA_STAT,#PRINT, if_update
	push #RSSI_PRINT
	push #FREQ_PRINT
	bres RDA_STAT,#PRINT
	#ENDIF
if_update:
	btjf RDA_STAT,#UPDATE, loop
	push #SPACE_BAND
	push #CTRL_REG_UPDATE			; update control register
	push #CHECK_TUNE			; check tuning
	push #RDS_REG_UPDATE		; update rds registers
	bres RDA_STAT,#UPDATE
loop:
	pop a
	tnz a
	jrmi loop_delay
	sll a
	x=a
	ldw x,(states,x)
	call (x)
	jra loop
loop_delay:
	#IF WDOG
	mov IWDG_KR, #$aa  				; reset watchdog
	#ENDIF
	btjf RDA_STAT,#READRDS, delay_250
	call rda5807m_rds_update
	call log_rds
	ldw x,#43
	jra to_delay
delay_250:
	ldw x,#250
to_delay:
	call delay
	push #$ff
    jra start
	;------ subroutines --------------
UPDATE_RDS_REGISTERS:
	#IF USE_UART
	btjf RDA_STAT,#DEBUG,no_msg_update_rds
	print_str msg_update_rds		; print message: "Read RDS Registers"
	#ENDIF
no_msg_update_rds
	call rda5807m_rds_update
	ret
	;---------------------------------
CHECK_TUNING:
	btjt RDA5807M_RDS_H,#6, quit_update_rds
	call LONG_DELAY
	call UPDATE_RDS_REGISTERS
	jra CHECK_TUNING
quit_update_rds
	ret
	;-----------------------------------
UPDATE_CONTROL_REGISTERS:
	#IF USE_UART
	btjf RDA_STAT,#DEBUG,skip_msg_update_ctl
	print_str msg_update_ctl		; print message: "Read Control Registers"
	#ENDIF
skip_msg_update_ctl:
	call rda5807m_update			; read CONTROL block of rda5807m reg[0x02 - 0x07]
	#IF USE_UART
	btjf RDA_STAT,#DEBUG,skip_msg_update_complete
	print_str msg_update_complete	; print message "Read Registers was Complete""=
	#ENDIF
skip_msg_update_complete:
	ret
	;btjf RDA_STAT,#FIRST,store_band_and_space
	;bres RDA_STAT,#FIRST
	;#IF USE_UART
	;print_str msg_ready
	;#ENDIF
;---PRINT FREQUENCY of CURRENT STATION -----
;store_band_and_space:
	;bres RDA_STAT,#UPDATE				; if success, then 1) reset flag
;-- write current space and band ---------	
STORE_BAND:
	ld a,$13						; get low byte REG_03 /TUNER/
	and a, #$03						; mask bitfield [1:0]
	ld SPACE,a						; store SPACE value
	ld a,$13						; get low byte REG_03 /TUNER/
	and a,#$0c						; mask bitfield [3:2]
	srl a							; (a>>1), right logical shift
	srl a							; (a>>1), right logical shift
	cp a,#3
	jrne leave_50M
	btjt $1a,#1,leave_50M
	ld a,#4
leave_50M:
	ld BAND,a						; store BAND value
	ret
	;-------------------------------------
LONG_DELAY:
	ldw x,#250
	call delay
	ret
	;-----------------------------------
IS_READY:
	#IF USE_UART
	print_str msg_ready
	#ENDIF
	ret
	;----------------------------------
Enable_Interrupt:
	rim
	ret
	
	; ------ messages ----------------
msg_ready:
	STRING "RDA5807m is ready.",$0a,$00
msg_update_rds:
	STRING "Read RDS Registers",$0a,$00
msg_update_ctl:
	STRING "Read Control Registers",$0a,$00
msg_update_complete:
	STRING "Read Registers was Complete",$0a,$00
	
states:
	DC.W UPDATE_RDS_REGISTERS, UPDATE_CONTROL_REGISTERS, CHECK_TUNING, IS_READY
	DC.W Enable_Interrupt, print_freq, print_rssi, STORE_BAND
	end


