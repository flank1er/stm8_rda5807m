stm8/
    segment 'rom'
    ;----------- delay ---------------------------------
    ; input parameter: X - register
.delay:
	pushw x
	pushw y
delay_start
    ldw y, #4000      ; 1ms
delay_loop:
    subw y,#1
    jrne delay_loop
    decw x
    jrne delay_start
	popw y
	popw x
    ret

    end
	