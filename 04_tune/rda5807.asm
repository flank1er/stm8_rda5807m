stm8/
	INTEL
    #include "STM8S103F.inc"
	extern init_i2c, read_i2c, write_i2c
	
;-------- Constants ----------------------
RDA5807_CTRL equ 10h
RDA5807_RDS cequ {RDA5807_CTRL+10h}
RDA5807M_SEQ_I2C_ADDRESS equ 20h
RDA5807M_RND_I2C_ADDRESS equ 22h
RDA5807M_CTRL_REG equ 02h

enable_i2c	MACRO
	bset I2C_CR1,#0
	MEND
	
disable_i2c	MACRO
	bres I2C_CR1,#0
	MEND
	
stop_i2c MACRO
	bset I2C_CR2,#1         ; STOP
;---------------------------------------------------
    bres I2C_CR2,#7         ; set SWRST
	MEND
	
	segment 'rom'
	; ------- rda5807m_rds_update ----------------------
	; read six 16-bit registers [0a-0f] to buffer "RDA5807_RDS" (adr 0x20-0x2c]
.rda5807m_rds_update
	enable_i2c
	push #0ah						; select control register of rda5807m
	push #RDA5807M_RND_I2C_ADDRESS 	; =0x22
	call init_i2c
	addw sp,#02
	tnz a							; check return of init_i2c
	jrne rda5807m_rds_update_quit	; if (init_i2c != OK) then return with error
	stop_i2c						; else reading 12 bytes from rda5807m
	push #RDA5807_RDS				; buffer adr
	push #12						; read 12 bytes
	push #RDA5807M_RND_I2C_ADDRESS	; =0x22 
	call read_i2c
	addw sp,#03
	clr a							; return success
rda5807m_rds_update_quit:			; quit
	disable_i2c
	ret
	
	; ------- rda5807m_update ----------------------
	; read six 16-bit registers [02-07] to buffer "RDA5807_CTRL" (adr 0x10-0x1c]
.rda5807m_update
	enable_i2c
	push #02						; select control register of rda5807m
	push #RDA5807M_RND_I2C_ADDRESS 	; =0x22
	call init_i2c
	addw sp,#02
	tnz a							; check return of init_i2c
	jrne rda5807m_update_quit		; if (init_i2c != OK) then return with error
	stop_i2c						; else reading 12 bytes from rda5807m
	push #RDA5807_CTRL				; buffer adr
	push #12						; read 12 bytes
	push #RDA5807M_RND_I2C_ADDRESS	; =0x22 
	call read_i2c
	addw sp,#03
	clr a							; return success
rda5807m_update_quit:				; quit
	disable_i2c
	ret

	; ------- rda5807m_control_write ----------------------
	; write to 0x02 register aka "RDA5807M_CTRL_REG"
	; input argument: (sp+3)=high_byte, (sp+4)=low_byte
.rda5807m_control_write 
	enable_i2c
	ld a,(03,sp)
	push a
	push #RDA5807M_SEQ_I2C_ADDRESS	;=0x20
	call init_i2c
	addw sp,#02
	tnz a
	jrne rda5807m_control_write_quit
	ld a,(04,sp)
	call write_i2c
	stop_i2c
	clr a
rda5807m_control_write_quit:
	disable_i2c
	ret

	; ------- rda5807_write_register ----------------------
	; write 16-bit value to rda5807m register
	; input argument: (sp+3)=register
	; (sp+4)=high_byte, (sp+5)=low_byte
.rda5807m_write_register
	enable_i2c
	ld a,(03,sp) 					; value
	push a
	push #RDA5807M_RND_I2C_ADDRESS	; rda5807m I2C address
	call init_i2c
	addw sp,#02
	tnz a
	jrne rda5807m_write_register_quit
	ld a,(04,sp)
	call write_i2c					; write high byte of register
	ld a,(05,sp)
	call write_i2c					; write low byte of register
	stop_i2c
	clr a
rda5807m_write_register_quit
	disable_i2c
	ret
	
	end
