x=a MACRO
	clrw x
	ld xl,a
	MEND
y=a MACRO
	clrw y
	ld yl,a
	MEND
	#IF USE_UART
print_nl MACRO
	ld a, #$a              
    call uart1_print_char
	MEND
print_str MACRO msg
	ldw x, #msg
	call uart1_print_str
	MEND
print_str_nl MACRO msg
	ldw x, #msg
	call uart1_print_str_nl
	MEND
	#ENDIF	
shiftX6 MACRO
	push a
	ld a,#6
	LOCAL shift
shift:
	sllw x
	dec a
	jrne shift
	pop a
	MEND
	
LED equ 5
LEN	equ 10
EOL equ	LEN					; =Zero always	; adr=0x0a
RDSTXT	equ $30
RDSTXT2A equ $40
SCANBUF	cequ {RDSTXT2A+$40}	; 	adr=0x80
SCANID	cequ {RDSTXT+8}
PRESET cequ {SCANID+2}
PRESET_STAT cequ {PRESET+2}
PRESET_CODE	equ $aa
;-------- Variables ----------------------
STR	equ 0					; buffer[10bytes]
INDEX	cequ	{EOL+1}		; 1 byte		; adr=0x0b
READY	cequ	{INDEX+1}	; 1 byte		; adr=0x0c
RDA_STAT cequ   {READY+1}	; 1 byte		; adr=0x0d
BAND	cequ	{RDA_STAT+1}; 1 byte		; adr=0x0e
SPACE	cequ	{BAND+1}	; 1 byte		; ard=0x0f
;-------- Constants ----------------------
RDA5807_CTRL equ $10
RDA5807_RDS cequ {RDA5807_CTRL+$10}
RDA5807M_SEQ_I2C_ADDRESS equ $20
RDA5807M_RND_I2C_ADDRESS equ $22
RDA5807M_CTRL_REG equ $02
RDA5807M_TUNER_REG equ $03
RDA5807M_CMD_RESET equ $0002
RDA5807M_RDS_H	equ RDA5807_RDS
;------- RDA_STAT -------------------------
UPDATE	equ 0	; read registers of RDA5807m
PRINT	equ 1	; print frequent
;RSSI	equ 2	; not use
DEBUG	equ	3	; print debug  info
PRESET_MODE equ 4
;FIRST   equ 4	; begin
READRDS equ	5	; read rds 
DECODE	equ 6	; decoding RDS
SCAN	equ 7	; autoscan
;------------------------------------------
