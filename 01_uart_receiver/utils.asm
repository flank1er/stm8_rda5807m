stm8/
	INTEL
    segment 'rom'
    ;----------- delay ---------------------------------
    ; input parameter: X - register
.delay:
    ldw y, #0fa0h      ; =4000
delay_loop:
    subw y,#1
    jrne delay_loop
    decw x
    jrne delay
    ret
    end
	