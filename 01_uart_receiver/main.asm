stm8/
    #include "STM8S103F.inc"
	extern delay,uart1_print_str,uart1_print_num,uart1_print_char

LED equ 5
LEN	equ 10
EOL equ	LEN			; =Zero always
;-------- Variables ----------------------
STR	equ 0					; buffer[10bytes]
INDEX	cequ	{EOL+1}	; 1 byte
READY	cequ	{INDEX+1}	; 1 byte

    segment 'rom'
.main
    ;----------- Setup Clock ----------------------
    ; Setup fHSI = 16MHz
    clr CLK_CKDIVR
    ; Enable UART and turn off other  peripherals   
    mov CLK_PCKENR1, #0
    mov CLK_PCKENR2, #0
    bset CLK_PCKENR1, #3    ; enable UART1
    ;----------- Setup GPIO -----------------------
    bset PB_DDR, #LED       ; PB_DDR|=(1<<LED)
    bset PB_CR1, #LED       ; PB_CR1|=(1<<LED)
    ;----------- Setup UART1 ----------------------
    ; Clear
    clr UART1_CR1
    clr UART1_CR2
    clr UART1_CR3
    clr UART1_CR4
    clr UART1_CR5
    clr UART1_GTR
    clr UART1_PSCR
    ; Setup UART1, set 115200 Baud Rate
    bset UART1_CR1, #5      ; set UARTD, UART1 disable
    ; 9600 Baud Rate
    ;mov UART1_BRR2, #0x03
    ;mov UART1_BRR1, #0x68
    ; 115200 Baud Rate
    mov UART1_BRR2, #$0b
    mov UART1_BRR1, #$08   
    ; 230400 Baud Rate
    ;mov UART1_BRR2, #0x05
    ;mov UART1_BRR1, #0x04
    ; 921600 Baud Rate
    ;mov UART1_BRR2, #0x01
    ;mov UART1_BRR1, #0x01
    ; Trasmission Enable
	bset UART1_CR2, #3      ; set TEN, Transmission Enable
    bset UART1_CR2, #2      ; set REN, Receiver Enable
    bset UART1_CR2, #5      ; set RIEN, Enable Receiver Interrupt 
    ; enable UART1
    bres UART1_CR1, #5      ; clear UARTD, UART1 enable
    ;------------- End Setup ---------------------
	clr EOL					;set NULL/EOL
start:
    clr INDEX               ; INDEX=0
    clr READY               ; READY=0

    ; let's go...
    rim                     ; enable Interrupts

mloop:
    ; receive string
    btjf READY,#0,main_no_receie
    ; print("count: ")
    ldw x, #msg_count
    call uart1_print_str
    clrw x
    ld a, INDEX
    ld xl,a
    call uart1_print_num    ; print count of symbols
    ldw x,#msg_line
    call uart1_print_str
    ldw x,#STR
    call uart1_print_str    ; print received string
    ld a, #$a              
    call uart1_print_char
    jra start
main_no_receie:
    bcpl PB_ODR, #LED       ; PB_ODR^=(1<<LED)
	ldw x,#500				; delay(500ms)
	call delay
	ldw x,#msg_line
    jp mloop

msg_count:
    STRING "count: "
    DC.B $00
msg_line:
    DC.B $0a
    STRING "line: "
    DC.B $00
	
    end
	

