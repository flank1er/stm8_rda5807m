stm8/
	INTEL
    #include "STM8S103F.inc"

	segment 'rom'

.init_i2c:
    ;----------- Begin  I2C routine ------------------
	; uint8_t init_i2c(uint8_t adr, uint8_t data);
    ;   Send Address
    bset I2C_CR2,#0         ; START
wait_start_tx:              ; wait SB in I2C_SR1
    btjf I2C_SR1, #0, wait_start_tx
    ld a,I2C_SR1            ; Clear SB bit
    ld a, (03,sp)
    ld I2C_DR,a             ; send I2C address
wait_adr_tx:
    btjt I2C_SR2,#2, fall_init_i2c ; if NACK
    btjf I2C_SR1,#1, wait_adr_tx
    ld a,I2C_SR1            ; clear ADDR bit
    ld a,I2C_SR3            ; clear ADDR bit
    ld a,(04,sp)
    ld I2C_DR,a             ; send data
wait_zero_tx:               ; wait set TXE bit
    btjf I2C_SR1,#7, wait_zero_tx
;--------------------------------------------------
    ld a, #0                ; return OK
    ret
fall_init_i2c:
    ld a,#1
    ret
	
	;----------- read array from I2C --------------------------
	;  void read_i2c(uint8_t adr, uint8_t count, uint8_t *data);
.read_i2c:
	push a
	pushw x
;--------------------------------------------------
    bset I2C_CR2,#2         	; set ACK bit
    bset I2C_CR2,#0         	; START
wait_start_rx:              	; wait SB in I2C_SR1
    btjf I2C_SR1, #0, wait_start_rx
    ld a,I2C_SR1            	; Clear SB bit
    ld a,(06,sp)				; a=i2c_address
    inc a						; read mode
    ld  I2C_DR, a           	; send i2c adr
wait_adr_rx:
    btjf I2C_SR1,#1, wait_adr_rx
    ; --------- READ BYTES --------------------
    bset I2C_CR2,#2         	; send ACK
    ld a,I2C_SR1            	; clear ADDR bit
    ld a,I2C_SR3            	; clear ADDR bit
    ld a,(08,sp)				; begin of buffer
	clrw x
	ld xl,a
    dec (07,sp)					; count-1
    ; --------- READ LOOP ----------------------
wait_read:
    btjf I2C_SR1,#6,wait_read
    ld a,I2C_DR					; read i2c data
    ld (x),a					; store date to buffer
    incw x
	dec (07,sp)					; decrement counter
    jrne wait_read				; if counter not equal zero then read again
    ;--  get last byte ---		; else send NACK and read last byte
    bres I2C_CR2,#2         	; NACK
    bset I2C_CR2,#1         	; STOP
wait_last:                      ; wait RXNE bit
    btjf I2C_SR1,#6, wait_last
    ld a,I2C_DR             	; get last byte
    ld (x), a               	; store date to buffer
    bres I2C_CR2,#7         	; set SWRST
	;--------------------------------------------------
	popw x
	pop a
    ret

	
	;----------- write byte to I2C --------------------------
    ; void write_i2c(uint8_t value);
	; input parameter: A - register
.write_i2c:
    ;ld a,(03,sp)
    ld I2C_DR,a             ; send data
write_i2c_loop:             ; wait set TXE bit
    btjf I2C_SR1,#7, write_i2c_loop
    ret


	end
	