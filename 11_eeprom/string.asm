stm8/
	
    segment 'rom'
	INTEL
	;----------- convert string to number, integer part -------------
	; parameters: X - input string; A - output num
.strnum:
	pushw x
	pushw y
	clrw y				; y=0
strnum_loop:
	ld a,(x)
	jreq strnum_quit	; if a==0 then EOL
	cp a,#'.'
	jreq strnum_quit	; if end of interger part
	cp a,#39h
	jrugt strnum_next	; if not digit then skip
	sub a,#30h
	jrslt strnum_next	; if not digit then skip
	push a
	ld a,#10
	mul y,a				; y = y * 10
	ld a,yl				; y = y % 256
	add a,(1,sp)		; y = y + a
	ld yl,a
	pop a
strnum_next:
	incw x
	jra strnum_loop		
strnum_quit:
	ld a,yl				; return y
	popw y
	popw x
	ret
	
	;----------- eject fraction part fron string  -------------
	; parameters: X - input string; A - output num
.strfrac:
	ld a,#'.'
	cp a,(x)
	jrne strfrac_next
	pushw x
	incw x
	callr strnum
	popw x
	ret
strfrac_next:
	tnz (x)
	jreq strfrac_quit
	incw x
	jra strfrac
strfrac_quit
	clr a
	ret

    ;----------- compare two strings ---------------------------------
    ; input parameter: X - first string, Y = second string
	; output - Accumulator
.strcmp:
	pushw x
	pushw y
strcmp_start:
	ld a,(y)
	jrne strcmp_nozero
	ld a,(x)
strcmp_quit_notok:
	popw y
	popw x
	ret
strcmp_nozero:
	cp a,(x)
	jrne strcmp_quit_notok
	incw x
	incw y
	jra strcmp_start
	

	end
	