stm8/
	#include "STM8S103F.inc"
	#include "rda5807.inc"

	extern rda5807m_control_write, rda5807m_write_register, rda5807m_get_readchan
	#IF USE_UART
	extern uart1_print_str, uart1_print_num,  uart1_print_char, uart1_print_hex
	#ENDIF
	extern delay, strfrac, calc_date, strnum, strcmp

	segment 'rom'
cmd_mute:
	bres RDA5807_CTRL,#6		; clear DMUTE bit
	ldw x,RDA5807_CTRL			; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write	; write to REG_02
	addw sp,#2
	bset RDA_STAT,#UPDATE		; update
	#if USE_UART
	print_str msg_mute			; print info message
	#ENDIF
	ret	
	
cmd_unmute:
	bset RDA5807_CTRL,#6		; set DMUTE bit
	ldw x,RDA5807_CTRL			; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write ; write to REG_02
	addw sp,#2
	bset RDA_STAT,#UPDATE		; update
	#IF USE_UART
	print_str msg_unmute		; print info message
	#ENDIF
	ret							; break

cmd_bass_on:
	bset RDA5807_CTRL,#4		; set BASS flag
	ldw x,RDA5807_CTRL			; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write	; write to REG_02
	addw sp,#2
	bset RDA_STAT,#UPDATE		; update
	#IF USE_UART
	print_str msg_bass_on		; print info message
	#ENDIF
	ret		
	
cmd_bass_off:
	bres RDA5807_CTRL,#4		; reset BASS flag
	ldw x,RDA5807_CTRL			; x=REG_02 /CONTROL/
	pushw x
	call rda5807m_control_write	; write to REG_02
	addw sp,#2
	bset RDA_STAT,#UPDATE		; update
	#IF USE_UART
	print_str msg_bass_off		; print info message
	#ENDIF
	ret

cmd_set_threshold:
	#IF USE_UART
	print_str msg_threshold
	#ENDIF
	ld a,$16					; load high byte REG_4 to accumulator
	and a,#$f0					; clear bitfield [14:10]
	ld $16,a					; return value from accumulator
	ldw x,#02
	call strnum					; get input value
	and a,#$0f					; set mask for bitfield [4:0]
	x=a
	#IF USE_UART
	call uart1_print_num
	#ENDIF
	or a,$16					; add with high byte REG_5
	ld $16,a
	ldw x,$16					; 
	pushw x
	push #5
	call rda5807m_write_register; write REG_4
    addw sp,#03
	bset RDA_STAT,#UPDATE		; update
	#IF USE_UART
	print_nl
	#ENDIF
	ret							; break
	
cmd_set_soft_blend:
	#IF USE_UART
	print_str msg_soft_blend
	#ENDIF
	ld a,$1a					; load high byte REG_7 to accumulator
	and a,#$83					; clear bitfield [14:10]
	ld $1a,a					; return value from accumulator
	ldw x,#02
	call strnum					; get input value
	and a,#$1f					; set mask for bitfield [4:0]
	x=a
	#IF USE_UART
	call uart1_print_num
	#ENDIF
	sll a						; left  shift to two bits
	sll a
	or a,$1a					; add with high byte REG_7
	ld $1a,a
	ldw x,$1a
	pushw x
	push #7
	call rda5807m_write_register; write REG_7
    addw sp,#03
	bset RDA_STAT,#UPDATE			; update
	#IF USE_UART
	print_nl
	#ENDIF
	ret							; break
	
on_cmd:
	#IF USE_UART
	print_str msg_on			; print info message
	#ENDIF
	ldw x,RDA5807_CTRL
	ld a,xl
	or a,#RDA5807M_CMD_RESET    ; set RESET bit
	ld xl,a
	pushw x
	call rda5807m_control_write ; write to REG_02 /CONTROL/
	addw sp,#2
	ldw x,#$c10d				; REG_02=0xC10D (Turn_ON + SEEK)
	pushw x
	call rda5807m_control_write ; write to REG_02 /CONTROL/
	addw sp,#2
	bset RDA_STAT,#UPDATE			; update
	#IF USE_UART
	bset RDA_STAT,#PRINT			; print freq
	#ENDIF
	ret	
	
vol_down:
	ld a,$17					; a=ram[0x17] (low byte REG_05 /VOLUME/)
	and a,#$0f					; mask
	jreq vol_min				; if current volume is minimum(=0)
	;------------
	#IF USE_UART
	print_str msg_volume		; print info message  "volume="
	#ENDIF
	dec a						; volume -=1
	#IF USE_UART
	x=a
	call uart1_print_num		; print volume
	print_nl					; print NL
	#ENDIF
	;----------------
	ldw x,$16					; x=REG_05 /VOLUME/
	decw x						; volume down
	pushw x
	push #05
	call rda5807m_write_register; write X to REG_05 /VOLUME/
	addw sp,#03
	bset RDA_STAT,#UPDATE		; update
	ret							; break
vol_min:
	#IF USE_UART
	print_str msg_volume_min	; print error message
	#ENDIF
	ret

vol_up:
	ld a,$17					; a=ram[0x17] (low byte REG_05 /VOLUME/)
	and a,#$0f					; mask
	cp a,#$0f					; if (a==15)
	jreq vol_max				; if current volume is maximum(=15)
	;------------
	#IF USE_UART
	print_str msg_volume		; print info message "volume="
	#ENDIF
	inc a
	#IF USE_UART
	x=a	
	call uart1_print_num		; print volume
	print_nl					; print NL
	#ENDIF
	;----------------
	ldw x,$16					; x=REG_05 /VOLUME/
	incw x						; vlume up
	pushw x
	push #05
	call rda5807m_write_register; write X to REG_05 /VOLUME/
	addw sp,#03
	bset RDA_STAT,#UPDATE		; set "to update" flag
	ret					; break
vol_max:
	#IF USE_UART
	print_str msg_volume_max	; print error message
	#ENDIF
	ret
	
volume:
	; get current Gain Control Bits(volume)
	#IF USE_UART
	print_str msg_volume		; print "volume="
	#ENDIF
	ld a,$17					; load low byte of REG_5 /VOLUME/
	and a,#$0f					; mask
	x=a
	#IF USE_UART
	call uart1_print_num		; print volume
	print_nl					; print NewLine
	#ENDIF
	ret							; break

set_vol:
	ldw x,#02
	call strnum					; get integer part
	and a,#$0f					; mask parameter
	ldw x,$16					; x=REG_5 /VOLUME/
	push a
	ld a,xl
	and a,#$f0					; x=(x & 0xfff0)
	or a,(1,sp)					; x=(x | num)
	ld xl,a						
	pop a
	pushw x
	push #05					; select REG_5 to write
	call rda5807m_write_register; write volume to REG_5 /VOLUME/
	addw sp,#03
	bset RDA_STAT,#UPDATE		; update
	ret							; break
	
debug_on:
	#IF USE_UART
	print_str msg_debug_on		; print message: "Debug is ON"
	#ENDIF
	bset RDA_STAT,#DEBUG		; set debug flag
	ret							; break
	
debug_off:
	#IF USE_UART
	print_str msg_debug_off		; print message: "Debug is OFF"
	#ENDIF
	bres RDA_STAT,#DEBUG		; reset debug flag
	ret							; break
	
raw_on:
	#IF USE_UART
	print_str msg_raw_rds_log_on	; print message: "Debug is ON"
	#ENDIF
	bset RDA_STAT,#READRDS		; set debug flag
	ret							; break

raw_off:
	#IF USE_UART
	print_str msg_raw_rds_log_off	; print message: "Debug is OFF"
	#ENDIF
	bres RDA_STAT,#READRDS		; reset debug flag
	ret					; break
	
log_on:
	#IF USE_UART
	print_str msg_rds_log_on	; print message: "Debug is ON"
	#ENDIF
	bset RDA_STAT,#READRDS		; set debug flag
	bset RDA_STAT,#DECODE		; set log flag
	ret							; break
	
log_off:
	#IF USE_UART
	print_str msg_rds_log_off	; print message: "Debug is OFF"
	#ENDIF
	bres RDA_STAT,#READRDS		; reset debug flag
	bres RDA_STAT,#DECODE		; reset log flag
	ret							; break

rst:
	bset $11,#1					; set RESET flag
	ldw x,$10					; load CONTROL reg to X
	pushw x
	call rda5807m_control_write ; write X to REG_02 /CONTROL/
	addw sp,#2
	;reboot microcontroller
	;print_str msg_reset			; print message "Reset"
	;mov RDA_STAT,#$10
	#IF WDOG
	mov IWDG_KR, #$cc;
    mov IWDG_KR, #$55;       	; unlock IWDG_PR & IWDG_RLR
    mov IWDG_PR, #6        		; =256
    mov IWDG_RLR, #1
	mov IWDG_KR, #$aa       	; lock IWDG_PR & IWDG_RLR
rst_loop:
	jra rst_loop
	#ENDIF
	ret
	;jp start					; break
	
help:
	#IF USE_UART
	print_str msg_ready			; print help message
	#ENDIF
	ret							; break
	
scan:
	#IF USE_UART
	print_str msg_scan
	#ENDIF
	bset RDA_STAT,#SCAN
	ld a,BAND
	call set_band
	clrw x
	ldw SCANID,x
	ret
.scan_loop:
	ldw y,RDA5807_RDS
	ld a,yh
	and a,#03
	ld yh,a
	ldw x,SCANID
	jreq scan_first
	decw x
	sllw x
	cpw y,(SCANBUF,x)
	jruge scan_quit
	ldw x,SCANID
	sllw x
scan_first:
	ldw (SCANBUF,x),y
	;ldw x,(SCANID)
	;incw x
	;ldw SCANID,x
	inc {SCANID+1}
	jp seek_down
scan_quit:
	dec {SCANID+1}
	;ld a,{SCANID+1}
	ld a,#$ff
left_loop:
	inc a
	x=a
	sllw x
	ldw x,(SCANBUF,x)
	pushw x
	cp a,{SCANID+1}
	jrc left_loop
	
	ld a,#$ff
right_loop:
	inc a
	x=a
	sllw x	
	ld (SCANBUF,x),a
	inc (SCANBUF,x)
	popw y
	ld yh,a
	ld a,yl
	;incw x
	ld ({SCANBUF+1},x),a
	ld a,yh
	cp a,{SCANID+1}
	jrc right_loop
	
	bres RDA_STAT,#SCAN
	#IF USE_UART
	print_str msg_found
	ldw x,SCANID
	incw x
	call uart1_print_num
	print_str msg_stations
	#ENDIF
	ldw x,SCANID
	ld a,xl
	sll a
	inc a
	ld xl,a
	ld a, (SCANBUF,x)
	pushw x
hash_loop:
	tnzw x
	jreq hash_quit
	decw x
	xor a,(SCANBUF,x)
	jra hash_loop
hash_quit:
	popw x
	incw x
	ld (SCANBUF,x),a
	call check_hash
	;-----------------------------
	ld a,PRESET
	clr PRESET
	cp a,#PRESET_CODE
	jrne eeprom_fault
	btjt FLASH_IAPSR,#3, eeprom_write	; if DUL flag is set
	; da-damm...
	ldw x,#50
	call delay
	; unlock eeprom
	mov FLASH_DUKR,#$ae
	mov FLASH_DUKR,#$56
wait_mass_off:
	btjf FLASH_IAPSR,#3,wait_mass_off
eeprom_write:
	ld a,{SCANID+1}
	ld EEPROM,a
	inc a
	x=a
	sllw x
	ld a,(SCANBUF,x)
	ld {EEPROM+1},a
	;mov EEPROM,{SCANID+1}
	;mov {EEPROM+1},PRESET
	ld a,#$ff
eeprom_loop:
	inc a
	x=a
	push a
	sllw x
	ld a,(SCANBUF,x)
	ld ({EEPROM+2},x),a
	incw x
	ld a,(SCANBUF,x)
	ld ({EEPROM+2},x),a
	pop a
	cp a,{SCANID+1}
	jrne eeprom_loop
	; lock eeprom
	bres FLASH_IAPSR,#3
eeprom_fault:
	ret
							; break
	
seek_down:
	btjf RDA_STAT,#PRESET_MODE,seek_down_no_preset
	ld a,PRESET
	cp a,#PRESET_CODE
	jrne seek_down_no_preset
	ld a,PRESET_STAT
	cp a,#1
	jrne seek_down_preset_not_zero
	jp preset4
seek_down_preset_not_zero:
	dec a
	ld PRESET_STAT,a
	dec a
	call get_preset_stat
	clr a
	ld xh,a
	ld a,SPACE
	push a
	jp space_recalc
seek_down_no_preset:
	bres RDA5807_CTRL,#1		; seek-down
	bset RDA5807_CTRL,#0		; seek enable
	ldw x,RDA5807_CTRL			; X = ram[0x10]
	pushw x
	call rda5807m_control_write ; write X to REG_02 /CONTROL/
	addw sp,#2
	bset RDA_STAT,#UPDATE		; set "to update" flag
	#IF USE_UART
	btjt RDA_STAT,#SCAN,seek_down_quit
	bset RDA_STAT,#PRINT		; print freq
	print_str msg_seekdown		; print info message
	#ENDIF
seek_down_quit
	ret							; break


set_band:
	sll a
	sll a
	or a,SPACE
	or a,#$10					; set TUNE flag
	x=a
	pushw x
	push #03
    call rda5807m_write_register; write REG_3 /TUNE/
    addw sp,#03
	ldw x,#200			 			; delay(200ms)
	call delay
	jra seek_down

band_ws:
	#IF USE_UART
	print_str msg_change_band
	print_str msg_band_eu
	#ENDIF
	clr a
	jra set_band
	
band_ww:
	#IF USE_UART
	print_str msg_change_band
	print_str msg_band_ww
	#ENDIF
	ld a,#2
	jra set_band

band_ukv:
	#IF USE_UART
	print_str msg_change_band
	print_str msg_band_ukv
	#ENDIF
	btjt $1a,#1,omit_50M_65M
	bset $1a,#1
	ldw x,$1a
	pushw x
	push #7
	call rda5807m_write_register; write REG_7
    addw sp,#03
omit_50M_65M:
	ld a,#3
	jra set_band

band_jp:
	#IF USE_UART
	print_str msg_change_band
	print_str msg_band_jp
	#ENDIF
	ld a,#01
	jra set_band
	
band_50M:
	#IF USE_UART
	print_str msg_change_band
	print_str msg_band_50M
	#ENDIF
	btjf $1a,#1,omit_65M_50M
	bres $1a,#1
	ldw x,$1a
	pushw x
	push #7
	call rda5807m_write_register; write REG_7
    addw sp,#03
omit_65M_50M:
	ld a,#3
	jp set_band

space_up:
	ld a,SPACE
	inc a
	and a,#3
	push a
;-- CASE (space)
	sll a	
	x=a
	ldw x,(case_recalc,x)
	jp (x)	
case_recalc:
	DC.W space_100,space_200,space_50,space_25
space_100:
	#IF USE_UART
	print_str msg_space100
	#ENDIF
	call rda5807m_get_readchan
	srlw x
	srlw x
	jra space_recalc
space_25:
	#IF USE_UART
	print_str msg_space25
	#ENDIF
	call rda5807m_get_readchan
	sllw x
	jra space_recalc
space_50:
	#IF USE_UART
	print_str msg_space50
	#ENDIF
	call rda5807m_get_readchan
	sllw x
	sllw x
	jra space_recalc
space_200:
	#IF USE_UART
	print_str msg_space200
	#ENDIF
	call rda5807m_get_readchan
	srlw x
space_recalc:
	shiftX6
	ld a,BAND
	cp a,#4
	jrne not_50M_band
	ld a,#3
not_50M_band:
	sll a
	sll a
	or a,(1,sp)
	pushw x
	or a,(2,sp)
	or a,#$10					; set TUNE flag
	ld xl,a
	addw sp,#03
	pushw x
	push #03
    call rda5807m_write_register; write REG_3 /TUNE/
    addw sp,#03
    bset RDA_STAT,#UPDATE       ; to update
	#IF USE_UART
    bset RDA_STAT,#PRINT        ; print freq
	#ENDIF
	ret							; break
	
space_down:
	ld a,SPACE
	dec a
	and a,#3
	push a
;-- CASE (space)
	sll a	
	x=a
	ldw x,(case_recalc2,x)
	jp (x)	
case_recalc2:
	DC.W space2_100,space2_200,space2_50,space2_25
space2_100:
	#IF USE_UART
	print_str msg_space100
	#ENDIF
	call rda5807m_get_readchan
	sllw x
	jra space_recalc
space2_200:
	#IF  USE_UART
	print_str msg_space200
	#ENDIF
	call rda5807m_get_readchan
	srlw x
	srlw x
	jra space_recalc
space2_50:
	#IF USE_UART
	print_str msg_space50
	#ENDIF
	call rda5807m_get_readchan
	srlw x
	jra space_recalc
space2_25:
	#IF USE_UART
	print_str msg_space25
	#ENDIF
	call rda5807m_get_readchan
	sllw x
	sllw x
	jra space_recalc

seek_up:
	btjf RDA_STAT,#PRESET_MODE,seek_up_no_preset
	ld a,PRESET
	cp a,#PRESET_CODE
	jrne seek_up_no_preset
	ld a,EEPROM
	inc a
	cp a,PRESET_STAT
	jrne seek_up_preset_not_zero
	clr a
	jra seek_up_zero
seek_up_preset_not_zero:
	ld a,PRESET_STAT
	inc PRESET_STAT
	call get_preset_stat
	clr a
	ld xh,a
	ld a,SPACE
	push a
	jp space_recalc
seek_up_zero
	call get_preset_stat
	clr a
	ld xh,a
	ld PRESET_STAT,a
	inc PRESET_STAT
	ld a,SPACE
	push a
	jp space_recalc
seek_up_no_preset:
	bset RDA5807_CTRL,#1		; seek-up
	bset RDA5807_CTRL,#0		; seek enable
	ldw x,RDA5807_CTRL			; X = ram[0x10]
	pushw x
	call rda5807m_control_write ; write X to REG_02 /CONTROL/
	addw sp,#2
	bset RDA_STAT,#UPDATE		; set "to update" flag
	#IF USE_UART
	bset RDA_STAT,#PRINT			; print freq
	print_str msg_seekup		; print info message
	#ENDIF
	ret	


freq:
	; CHECK: if was recived "f=NUM.NUM" command
	ldw x,#02
	call strnum					; get integer part
	y=a							; y = integer part
	;===========================================
	btjf RDA_STAT,#PRESET_MODE,freq_no_preset
	ld a,PRESET
	cp a,#PRESET_CODE
	jrne freq_no_preset
	ld a,yl
	tnz a
	jreq freq_invalid
	dec a
	cp a,EEPROM
	jrugt freq_invalid
	ld PRESET_STAT,a
	inc PRESET_STAT
	call get_preset_stat
	clr a
	ld xh,a
	ld a,SPACE
	push a
	jp space_recalc
freq_invalid:
	print_str msg_invalid
	ret
	;======================
freq_no_preset:
	call strfrac				; get fractional part
	push a						; fractional part to stack
	ld a,BAND
	sll a
	x=a
	ldw  x,(band_range,x)		; x=low edge of current band
	pushw x
	subw y,(1,sp)				; y = (integer part - low edge of current band)
	ld a,SPACE
	x=a
	ld a,(spaces,x)				; load scaler
	mul y,a						; y = y * scaler
	ld a,(3,sp)
	x=a							; x = fractional part
	pushw y						; save y
	ld a,SPACE
	sll a
	y=a
	ldw y,(case_freq,y)
	jp (y)
case_freq: 
	DC.W sp_is_0, sp_is_1, sp_is_2, sp_is_3
sp_is_3
	sllw x
	incw x
	ld a,#5
	div x,a
	jra sp_is_0	
sp_is_2:
	ld a,#5
	div x,a						; x=x/5
	jra sp_is_0
sp_is_1:
	srlw x
sp_is_0:
	addw x,(1,sp)				; x=x+y
	addw sp,#5
	push SPACE
	jp space_recalc

.print_freq:
	#IF USE_UART
	btjf RDA_STAT,#PRESET_MODE,print_freq_no_preset
	ld a,PRESET
	cp a,#PRESET_CODE
	jrne print_freq_no_preset
	call get_current_stat
	ld a,xh
	jp print_station
print_freq_no_preset:
	call rda5807m_get_readchan
print_freq2:
	ld a,SPACE
	y=a 
	ld a,(spaces,y)					; load current scaler
	y=a								; load scaler to Y reg
	divw x,y						; X = X / Scaler
	pushw x
	ld a,SPACE
	sll a
	x=a
	ldw x,(chose_space,x)
	jp (x)	
chose_space:
	DC.W sp_0,sp_1,sp_2,sp_3
sp_1:
	sllw y
	jra sp_0
sp_2:
	ld a,#5
	mul y,a
	jra sp_0
sp_3:
	ld a,#25
	mul y,a
sp_0:
	print_str msg_freq				; print X
	ld a,BAND
	sll a
	x=a
	ldw x,(band_range,x)
	addw x,(1,sp)
	addw sp,#2
	;popw x
	;addw x,band_range				; X = X + low range of current band
	call uart1_print_num
	ld a,#'.'
	call uart1_print_char			; print dot
	ldw x,y
	ld a,#3
	cp a,SPACE
	jrne print_fract
	cpw x,#100
	jrsge print_fract
	ld a,#'0'
	call uart1_print_char
print_fract:
	call uart1_print_num			; print fractional part of frequency
	print_str msg_mhz
	btjf RDA_STAT,#DEBUG, print_freq_quit
;---print space --------
	print_str msg_space
	clrw x
	ld a,SPACE
	ld xl,a
	call uart1_print_num
	print_nl
;---print band ---------
	print_str msg_band
	clrw x
	ld a,BAND
	ld xl,a
	call uart1_print_num
	print_nl
;---print chan value ----
	print_str msg_chan
	call rda5807m_get_readchan
	call uart1_print_num
	print_nl
	#ENDIF
;---print stereo mode ----
	;call print_stereo
print_stereo:
	btjt $20,#2,stereo			; check ST flag of 0x0a register
	#IF USE_UART
	print_str msg_mono			; print info message
	#ENDIF
print_freq_quit
	ret
stereo:
	#IF USE_UART
	print_str msg_stereo		; print info message
	#ENDIF
	ret
	
get_freq:
	bset RDA_STAT,#UPDATE		; update
	#IF USE_UART
	bset RDA_STAT,#PRINT		; print freq
	#ENDIF
	ret
	
cmd_mono	
.print_rssi:							; print rssi
	#IF USE_UART
	ld a,$22						; load high byte 0x0B reg to accumulator
	srl a							; a = (a >> 1)
	print_str msg_rssi
	clrw x
	ld xl,a
	pushw x
	call uart1_print_num			; print rssi
	popw x
	print_str msg_dbuv
	#ENDIF
	ret

.log_rds:
	#IF USE_UART
	pushw x
	pushw y
	push a
	btjf RDA_STAT,#DECODE, log_rds_raw
	ld a,$26
	and a,#$f8
	cp a,#$40				; 0x4A
	jreq log_rds_4a
	cp a,#$0				; 0x0A
	jrne switch_rds_00
	jp log_rds_0a
switch_rds_00:
	cp a,#$08				; 0x0B
	jrne switch_rds_01
	jp log_rds_0a
switch_rds_01:
	cp a,#$20				; 0x2A
	jreq log_rds_2a
	jp end_rds_log
;log_rds_0a:
	;push #$0a
	;jra log_rds_raw
;	jra log_rds_0a
;log_rds_2a:
	;push #$2a
	;jra log_rds_raw
	;print_str msg_radiotext
;	jp log_rds_2a
	jp end_rds_log

log_rds_4a:
	;push #$4a
log_rds_raw:
	ldw y,#$20
log_rds_loop:
	ldw x,y
	ldw x,(x)
	call uart1_print_hex
	ld a, #' '              
	call uart1_print_char
	addw y,#2
	cpw y,#$2c
	jrne log_rds_loop
	btjt RDA_STAT,#6, log_rds_print_time
	jp end_raw_log
log_rds_print_time:
	jp log_rds_print_4a
;	pop a
;	cp a,#$4a
;	jreq log_rds_print_4a
;	cp a,#$0a
;	jreq log_rds_print_0a
;	jp end_raw_log
log_rds_2a:
	ld a,$27
	and a,#$0f
	tnz a
	jrne log_2a_idx_not_zero
	tnz $3f
	jreq log_2a_idx_zero
	print_str msg_rdx
	ldw x,#RDSTXT2A
	#IF USE_UART
	call uart1_print_str
	print_nl
	#ENDIF
	jra log_2a_idx_zero
log_2a_wrong:
	call clr_rdstxt_2a
	jp end_rds_log
log_2a_idx_not_zero:
	push a
	sub a,$3f
	cp a,#2
	pop a
	jrnc log_2a_wrong
	ld $3f,a
	sll a
	sll a
	jra jump_01
log_2a_idx_zero:
	call clr_rdstxt_2a
jump_01
	add a,#RDSTXT2A
	y=a
	ldw x,$28
	ldw (y),x
	ldw x,$2a
	ldw (2,y),x
	clr (4,y)
	ld a,$27
	and a,#$0f
	cp a,#$f
	jreq log_2a_print_radiotext
	jp end_rds_log
log_2a_print_radiotext:
	print_str msg_rdx
	ldw x,#RDSTXT2A
	call uart1_print_str
	clr $3f
	jp end_raw_log
log_rds_0a:
;log_rds_print_0a:
	ld a,$27			; get index
	and a,#$3
	tnz a
	jrne log_rda_idx_not_zero
	call clr_rdstxt
	jra log_rda_idx_zero
log_rds_wrong:
	call clr_rdstxt
	jp end_rds_log
log_rda_idx_not_zero:
	push a
	sub a,$3e
	cp a,#2
	pop a
	jrnc log_rds_wrong
	ld $3e,a
	sll a
log_rda_idx_zero:
	add a,#RDSTXT
	y=a
	ldw x,$2a
	ldw (y),x
	ld a,$27			; get index
	and a,#$3
	cp a,#3
	jreq log_rda_idx_print
	jp end_rds_log
log_rda_idx_print:
	ldw x,#RDSTXT
	call uart1_print_str
	clr $3e
error_rds:
	jp end_raw_log
log_rds_print_4a:
	print_str msg_time
	; extract Hour
	ldw x,$29
	srlw x
	ld a,xl
	srl a
	srl a
	srl a
	x=a
	call uart1_print_num
	ld a,#':'
	call uart1_print_char
	; extract Minutes
	ldw x, $2a
	sllw x
	sllw x
	ld a,xh
	and a,#$3f
	x=a
	call uart1_print_num
	print_str msg_offset
	btjf $2b,#5,negative_offset
	ld a,#'+'
	call uart1_print_char
	jra log_rds_print_offset
negative_offset:
	ld a,#'+'
	call uart1_print_char
log_rds_print_offset:
	ld a,$2b
	and a,#$1f
	x=a
	call uart1_print_num
	ldw x,$28
	srlw x
	btjt $27,#1,log_rds_error_date
	ld a,$27
	and a,#$03
	swap a
	sll a
	sll a
	sll a
	jrc log_rds_error_date
	push a
	ld a,xh
	or a,(1,sp)
	ld xh,a
	pop a
	cpw x,#$e661				; 8 may 2020
	jrc log_rds_error_date
	pushw x
	print_str msg_is_date
	;pushw x
	clrw x
	pushw x
	call calc_date
	pop a
	x=a
	call uart1_print_num
	ld a, #'-'
    call uart1_print_char
	pop a
	x=a
	call uart1_print_num
	ld a, #'-'
    call uart1_print_char
	pop a
	x=a
	addw x,#1900
	call uart1_print_num
	ld a, #' '
    call uart1_print_char
	pop a
	inc a
	x=a
	call uart1_print_num
	jra end_raw_log
log_rds_error_date:
	print_str msg_error_date
end_raw_log:	
	print_nl
end_rds_log:
	pop a
	popw y
	popw x
	#ENDIF
	ret
	
.clr_rdstxt:
	pushw x
	push a
	ldw x, #{RDSTXT+8}
	ld a,#$20
fill:
	decw x				
	ld (x),a
	cpw x,#RDSTXT
	jrne fill
	clr $3e
	pop a
	popw x
	ret
	
.clr_rdstxt_2a:
	pushw x
	push a
	ldw x, #{RDSTXT2A+64}
	ld a,#$20
fill_2a:
	decw x				
	ld (x),a
	cpw x,#RDSTXT2A
	jrne fill_2a
	clr $3f
	pop a
	popw x
	ret
	
.get_state:
	#IF USE_UART
	; parameters: A return of state
	clrw x						; ard of buffer: x=0
	ldw y,#str_mute
	call strcmp					; check incoming command
	jrne unmute	
	clr a
	ret
unmute:
	ldw y,#str_unmute
	call strcmp					; check incoming command
	jrne nxt_rst	
	ld a,#1
	ret
nxt_rst:
	ldw y,#cmd_rst
	call strcmp					; check incoming command
	jrne autoscan
	ld a,#2
	ret
autoscan:
	ldw y,#cmd_scan
	call strcmp
	jrne skip
	ld a,#35
	ret
skip:
	ldw x,#cmd_set
	subw x,#2
	clrw y
	ld a,#2
	ldw y,(y)
parser_loop:
	inc a
	cp a,#34
	jreq parser_quit_loop
	addw x,#2
	cpw y,(x)
	jrne parser_loop
	ret
parser_quit_loop
	ld a,yh
	cp a,#'?'
	jrne parser_fail
	ld a,#34
	ret
parser_fail:
	ld a,#$ff
	#ENDIF
	ret
	
check_hash:
	ldw x,SCANID
	tnzw x
	jreq bad_hash
	ld a,xl
	sll a
	inc a
	ld xl,a
	ld a, (SCANBUF,x)
	pushw x
hash_loop2:
	tnzw x
	jreq hash_quit2
	decw x
	xor a,(SCANBUF,x)
	jra hash_loop2
hash_quit2:
	popw x
	incw x
	cp a, (SCANBUF,x)
	jrne bad_hash
	mov PRESET,#PRESET_CODE
	ret
bad_hash:
	clr PRESET
	ret
	
.eeprom_check_hash:
	ld a,EEPROM
	tnz a
	jreq eeprom_bad_hash
	sll a
	inc a
	x=a
	ld a, (EEPROM_BUF,x)
eeprom_hash_loop2:
	tnzw x
	jreq eeprom_hash_quit2
	decw x
	xor a,(EEPROM_BUF,x)
	jra eeprom_hash_loop2
eeprom_hash_quit2:
	cp a, {EEPROM+1}
	jrne eeprom_bad_hash
	mov PRESET,#PRESET_CODE
	ret
eeprom_bad_hash:
	clr PRESET
	ret

preset_off:
	#IF USE_UART
	print_str msg_preset_off
	#ENDIF
	bres RDA_STAT,#PRESET_MODE
	ret
	
print_eeprom:
	#IF USE_UART
	ld a,PRESET
	cp a,#PRESET_CODE
	jrne quit_print_eeprom
	clr a
loop_print_eeprom:
	x=a
	sllw x
	ldw x,(EEPROM_BUF,x)
	inc a
	call print_station
	cp a,EEPROM
	jrule loop_print_eeprom
quit_print_eeprom:
	#ENDIF
	ret
	
preset_on:
	ld a,PRESET
	cp a,#PRESET_CODE
	jrne quit_preset
	#IF USE_UART
	print_str msg_preset_on
	#ENDIF
	bset RDA_STAT,#PRESET_MODE
	call rda5807m_get_readchan
	pushw x
	ld a,#$ff
preset_loop:
	inc a
	cp a,EEPROM
	jrugt last_station
	x=a
	sllw x
	ldw x,(EEPROM_BUF,x)
	push a
	ld a,xh
	ld PRESET_STAT,a
	ld a,xl
	cp a,(3,sp)
	pop a
	jrc preset_loop
	cpw x,(1,sp)
	jreq if_was_found
	addw sp,#2
	clr a
	ld xh,a
	ld a,SPACE
	push a
	jp space_recalc
if_was_found:
	#IF USE_UART
	call print_station
	#ENDIF
	popw x
quit_preset:
	ret
last_station:
	addw sp,#2
preset4:
	ld a,EEPROM
	x=a
	sllw x
	ldw x,(EEPROM_BUF,x)
	inc a
	ld PRESET_STAT,a
	clr a
	ld xh,a
	ld a,SPACE
	push a
	jp space_recalc
	
print_station:
	push a
	pushw x
	x=a
	call uart1_print_num
	ld a,#':'
	call uart1_print_char
	ld a,#' '
	call uart1_print_char
	ld a,(2,sp)
	clrw x
	ld xl,a
	call print_freq2
	popw x
	pop a
	ret
	
get_current_stat:
	ld a,PRESET_STAT
	dec a
	x=a
	sllw x
	ldw x,(EEPROM_BUF,x)
	ret
	
get_preset_stat:
	x=a
	sllw x
	ldw x,(EEPROM_BUF,x)
	ret
	
.band_range:
	DC.W 87,76,76,65,50
.spaces:
	DC.B 10,5,20,40
	
	#IF USE_UART
msg_ready:
	STRING "RDA5807m is ready.",$0a,0
msg_mute:
	STRING "mute ON",$0a,0
msg_unmute:
	STRING "mute OFF",$0a,0
msg_bass_on:
	STRING "Bass On",$0a,0
msg_bass_off:
	STRING "Bass Off",$0a,0
msg_stereo:
	STRING "Stereo Mode",$0a,0
msg_mono:
	STRING "Mono Mode",$0a,0
msg_threshold
	STRING "Threshold = ",0
msg_soft_blend:
	STRING "soft blend threshold = ",0
msg_rssi:
	STRING "rssi: ",0
msg_dbuv:
	STRING " dBuV",$0a,0
msg_on:
	STRING "Turn on",$0a,0
msg_volume:
	STRING "volume=",0
msg_volume_max:
	STRING "volume is max",$0a,0
msg_volume_min:
	STRING "volume is min",$0a,0
msg_debug_on:
	STRING "Debug is ON",$0a,0
msg_debug_off:
	STRING "Debug is OFF",$0a,0
msg_raw_rds_log_on:
	STRING "RAW RDS logging is ON",$0a,0
msg_raw_rds_log_off:
	STRING "RAW RDS logging is OFF",$0a,0
msg_rds_log_on:
	STRING "RDS logging is ON",$0a,0
msg_rds_log_off:
	STRING "RDS logging is OFF",$0a,0
msg_seekup:
	STRING "Seek Up",$0a,0
msg_seekdown:
	STRING "Seek Down",$0a,0
msg_preset_on:
	STRING "preset mode is ON",$0a,0
msg_preset_off:
	STRING "preset mode is OFF",$0a,0
msg_change_band:
	STRING "Change band to ",$00
msg_band_ww:
	STRING "World-Wide Band (76-108MHz)",$0a,0
msg_band_jp:
	STRING "Japan Band (76-91MHz)",$0a,0
msg_band_ukv:
	STRING "exUSSR Band (65-76MHz)",$0a,0
msg_band_50M:
	STRING "50MHz Band (50-65MHz)",$0a,0
msg_band_eu:
	STRING "West Band (87-108MHz)",$0a,0
msg_space200:
	STRING "Change space to 200kHz",$0a,0
msg_space100:
	STRING "Change space to 100kHz",$0a,0
msg_space50:
	STRING "Change space to 50kHz",$0a,0
msg_space25:
	STRING "Change space to 25kHz",$0a,0
msg_space:
	STRING "Space: ",0
msg_band:
	STRING "Band: ",0
msg_mhz:
	STRING " MHz",$0a,0
msg_freq:
	STRING "freq=",0
msg_chan:
	STRING "chan: ",0
msg_error_date:
	STRING " Incorrect Date", $0a,0
msg_is_date:
	STRING " Date: ",0
msg_found:
	STRING "was found: ",0
msg_stations
	STRING " stations.",$0a,0
msg_is_station
	STRING "station is: ",0
msg_invalid
	STRING "invalid value!",$0a,0
msg_rdx:
	STRING "RDX: ",0
msg_radiotext:
	STRING "RADIOTEXT: ", $0a,0
msg_scan:
	STRING "Scanning.. ",$0a,0
msg_done:
	STRING "Done.",$0a,0
msg_offset
	STRING ", offset: ",0
msg_time
	STRING "time: ",0
str_mute:
	STRING "mute ON",0
str_unmute:
	STRING "mute OFF",0
cmd_rst:
	STRING "rst",0
cmd_scan:
	STRING "scan",0
cmd_set:
	STRING "b+b-?mt=b=ws50esjpwwd+d-r+r-l+l-c-c+v=v-v+s-s+?qonf=?v?fp+p-ps"		; 31 commands
	#ENDIF
.chose_state:
	DC.W cmd_mute,cmd_unmute, rst, cmd_bass_on, cmd_bass_off, cmd_mono, cmd_set_threshold
	DC.W cmd_set_soft_blend, band_ws, band_50M, band_ukv, band_jp, band_ww, debug_on, debug_off
	DC.W raw_on, raw_off, log_on, log_off, space_down, space_up,set_vol
	DC.W vol_down, vol_up, seek_down, seek_up, print_rssi, on_cmd, freq, volume
	DC.W get_freq, preset_on, preset_off, print_eeprom, help, scan
	end
